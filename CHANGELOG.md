
# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.1.4] - 2023-12-21

- Fixing `publication_date` issue [#26166]

## [v1.1.3] - 2023-07-25

- Fixing `null` issue, recompiling this component [#25275]

## [v1.1.2] - 2022-10-27

**Enhancements**

- [#24038] Moved to GWT 2.9.0
- Moved to maven-portal-bom 3.6.4

## [v1.1.0] - 2021-10-05

**New Features**
[#19988] Integrated with `checkEnvironment` reporting the status of "Upload to Zenodo" facility
Moved to `maven-portal-bom` 3.6.3

## [v1.0.2] - 2021-04-26

**Bug fixes**
[#21263] manage empty mapping for creators/contributors

**Enhancements**
[#21153] ported to maven-portal-bom 3.6.1
Improved modal height


## [v1.0.1] - 2021-03-09

[#20935] just to include new range 1.x, 2.0 of ckan2zenodo-library


## [v1.0.0] - 2020-08-26

#### New Features

[#19528] Ckan GUI to always allow zenodo publishing


## [v0.0.1] - 2019-12-06

[#18236] First Release
