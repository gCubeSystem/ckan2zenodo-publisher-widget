package org.gcube.portlets.widgets.ckan2zenodopublisher.client;

import java.util.Map;

import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.CatalogueItem;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.DOI_dv;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoItem;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Jan 28, 2020
 */
@RemoteServiceRelativePath("ckantozenodo")
public interface CkanToZenodoPublisherService extends RemoteService {

	/**
	 * Convert to zenodo item.
	 *
	 * @param item the item
	 * @return the zenodo item
	 * @throws Exception the exception
	 */
	ZenodoItem convertToZenodoItem(CatalogueItem item) throws Exception;

	/**
	 * Publish on zenodo.
	 *
	 * @param zenodoItem the zenodo item
	 * @return the DOI of published Zenodo Item
	 * @throws Exception the exception
	 */
	DOI_dv publishOnZenodo(ZenodoItem zenodoItem) throws Exception;

	/**
	 * Read fields descriptions.
	 *
	 * @return the map
	 * @throws Exception the exception
	 */
	Map<String, String> readFieldsDescriptions() throws Exception;

	/**
	 * Check zenodo environment.
	 *
	 * @return the true is Zenodo environment is configured in the operating scope,
	 *         false otherwise
	 */
	Boolean checkZenodoEnvironment();

}
