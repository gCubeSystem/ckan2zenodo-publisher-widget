package org.gcube.portlets.widgets.ckan2zenodopublisher.shared;

import java.io.Serializable;
import java.util.List;

/**
 * The Class SerializableEnum.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Dec 10, 2019
 * @param <T> the generic type
 */
public class SerializableEnum<E> implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<E> selectedValues;
	
	private List<E> selectableValues;


	public SerializableEnum() {
	}

	/**
	 * Instantiates a new serializable enum.
	 *
	 * @param enumerator the enumerator
	 */
	public SerializableEnum(List<E> selectedValues, List<E> selectableValues) {
		this.selectedValues = selectedValues;
		this.selectableValues = selectableValues;
	}

	public List<E> getSelectedValues() {
		return selectedValues;
	}

	public List<E> getSelectableValues() {
		return selectableValues;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SerializableEnum [selectedValues=");
		builder.append(selectedValues);
		builder.append(", selectableValues=");
		builder.append(selectableValues);
		builder.append("]");
		return builder.toString();
	}

}
