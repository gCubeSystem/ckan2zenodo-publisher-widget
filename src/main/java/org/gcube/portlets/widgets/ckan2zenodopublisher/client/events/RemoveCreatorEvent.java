package org.gcube.portlets.widgets.ckan2zenodopublisher.client.events;

import org.gcube.portlets.widgets.ckan2zenodopublisher.client.ui.authors.CreatorView;

import com.google.gwt.event.shared.GwtEvent;


/**
 * The Class RemoveCreatorEvent.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Dec 19, 2019
 */
public class RemoveCreatorEvent extends GwtEvent<RemoveCreatorEventHandler> {
	public static Type<RemoveCreatorEventHandler> TYPE = new Type<RemoveCreatorEventHandler>();

	private CreatorView creatorView;

	/**
	 * Instantiates a new removes the creator event.
	 *
	 * @param view the view
	 */
	public RemoveCreatorEvent(CreatorView view) {
		this.creatorView = view;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public Type<RemoveCreatorEventHandler> getAssociatedType() {
		return TYPE;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.EventHandler)
	 */
	@Override
	protected void dispatch(RemoveCreatorEventHandler handler) {
		handler.onAddedResource(this);
	}
	
	/**
	 * Gets the creator view.
	 *
	 * @return the creator view
	 */
	public CreatorView getCreatorView() {
		return creatorView;
	}
}
