package org.gcube.portlets.widgets.ckan2zenodopublisher.client.ui.authors;

import java.util.Arrays;
import java.util.List;

import org.gcube.portlets.widgets.ckan2zenodopublisher.client.events.RemoveCreatorEvent;
import org.gcube.portlets.widgets.ckan2zenodopublisher.client.ui.utils.InfoTextAndLabels;
import org.gcube.portlets.widgets.ckan2zenodopublisher.client.view.FieldUtil;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoAuthor;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoContributor;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoCreator;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.ControlGroup;
import com.github.gwtbootstrap.client.ui.ControlLabel;
import com.github.gwtbootstrap.client.ui.ListBox;
import com.github.gwtbootstrap.client.ui.TextBox;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Widget;


// TODO: Auto-generated Javadoc
/**
 * The Class CreatorView.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Jan 15, 2020
 */
public class CreatorView extends Composite {

	private static CreatorViewUiBinder uiBinder = GWT.create(CreatorViewUiBinder.class);

	/**
	 * The Interface CreatorViewUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
	 * 
	 * Jan 15, 2020
	 */
	interface CreatorViewUiBinder extends UiBinder<Widget, CreatorView> {
	}
	
	@UiField
	TextBox field_name;
	
	@UiField
	TextBox field_affiliation;
	
	@UiField
	TextBox field_orcid;
	
	@UiField
	Button remove_author;
	
	@UiField
	ControlGroup cg_authors;
	
	@UiField
	ListBox field_author_type;
	
	@UiField
	HorizontalPanel controls_author_type;
	
	@UiField
	ControlLabel field_cl_author;
	

	private HandlerManager eventBus;

	private String userRole; //Can be: Creator, Contributor etc..

	private ZenodoAuthor author;

	private boolean isMandatoryField;
	
	/**
	 * Instantiates a new creator view.
	 *
	 * @param author the author
	 * @param userRole the user role
	 * @param isMandatoryField the is mandatory field
	 */
	public CreatorView(ZenodoAuthor author, String userRole, Boolean isMandatoryField) {
		initWidget(uiBinder.createAndBindUi(this));
		GWT.log("Creating CreatorView for author: "+author+", userRole: "+userRole);
		this.userRole = userRole;
		this.author = author;
		this.isMandatoryField = isMandatoryField == null? false:isMandatoryField;
		
		String fieldLabel = isMandatoryField?"<font color='red'>*</font>":"";
		if(this.userRole!=null) {
			field_cl_author.add(new HTML(fieldLabel+"&nbsp;"+userRole));
			remove_author.setText("Remove "+userRole);
		}else { //default role is Author
			field_cl_author.add(new HTML(fieldLabel+"&nbsp;Author"));
			remove_author.setText("Remove Author");
		}
		
		if(this.author!=null) {
			setField_name(InfoTextAndLabels.validValue(this.author.getName()));
			setField_affiliation(InfoTextAndLabels.validValue(this.author.getAffiliation()));
			setField_orcid(InfoTextAndLabels.validValue(this.author.getOrcid()));
			
			if(this.author.getType()!=null) {
				controls_author_type.setVisible(true);
				FieldUtil.addValuesToListBox(field_author_type, this.author.getType().getSelectableValues());
				
				List<String> listSelectedValues = this.author.getType().getSelectedValues();
				
				if(listSelectedValues==null || listSelectedValues.isEmpty()) {
					//if no type is selected (e.g. when no contributor is returned), selecting the first value of selectable values
					listSelectedValues = Arrays.asList(this.author.getType().getSelectableValues().get(0));
				}else
					//selecting the first value of selected values 
					listSelectedValues = Arrays.asList(this.author.getType().getSelectedValues().get(0));
				
				FieldUtil.selectValueToListBox(field_author_type, listSelectedValues);
			}
		}
		
		if(this.userRole!=null) {
			String userRoleToLowerCase = userRole.toLowerCase();
		
			if(userRoleToLowerCase.startsWith(ZenodoCreator.USER_ROLE.toLowerCase())) {
				InfoTextAndLabels.addTooltipForFieldKey("creators", field_cl_author);
			}else if(userRoleToLowerCase.startsWith(ZenodoContributor.USER_ROLE.toLowerCase())) {
				InfoTextAndLabels.addTooltipForFieldKey("contributors", field_cl_author);
			}else {
				InfoTextAndLabels.addTooltipForFieldKey("authors", field_cl_author);
			}
		}
			
		GWT.log("Created creator view with author: "+this.author);
	}
	
	
	/**
	 * Instantiates a new creator view.
	 *
	 * @param author the author
	 * @param userRole the user role
	 * @param eventBus the event bus
	 * @param isFieldMandatory the is field mandatory
	 */
	public CreatorView(ZenodoAuthor author, String userRole, HandlerManager eventBus, Boolean isFieldMandatory) {
		this(author, userRole, isFieldMandatory);
		this.eventBus = eventBus;
		
		remove_author.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				CreatorView.this.eventBus.fireEvent(new RemoveCreatorEvent(CreatorView.this));
			}
			
		});
	}

	/**
	 * Sets the visible remove creator.
	 *
	 * @param visible the new visible remove creator
	 */
	public void setVisibleRemoveCreator(boolean visible) {
		remove_author.setVisible(visible);
	}
	
	
	/**
	 * Checks if is mandatory field.
	 *
	 * @return true, if is mandatory field
	 */
	public boolean isMandatoryField() {
		return isMandatoryField;
	}

	/**
	 * Gets the field name.
	 *
	 * @return the field name
	 */
	public TextBox getField_name() {
		return field_name;
	}

	/**
	 * Sets the field name.
	 *
	 * @param field_name the new field name
	 */
	public void setField_name(String field_name) {
		this.field_name.setValue(field_name);
	}

	/**
	 * Gets the field affiliation.
	 *
	 * @return the field affiliation
	 */
	public TextBox getField_affiliation() {
		return field_affiliation;
	}

	/**
	 * Sets the field affiliation.
	 *
	 * @param field_affiliation the new field affiliation
	 */
	public void setField_affiliation(String field_affiliation) {
		this.field_affiliation.setValue(field_affiliation);
	}

	/**
	 * Gets the field orcid.
	 *
	 * @return the field orcid
	 */
	public TextBox getField_orcid() {
		return field_orcid;
	}

	/**
	 * Sets the field orcid.
	 *
	 * @param field_orcid the new field orcid
	 */
	public void setField_orcid(String field_orcid) {
		this.field_orcid.setValue(field_orcid);
	}
	
	/**
	 * Gets the control group author.
	 *
	 * @return the control group author
	 */
	public ControlGroup getControlGroup_Author() {
		return cg_authors;
	}
	
	/**
	 * Gets the user role.
	 *
	 * @return the user role
	 */
	public String getUserRole() {
		return userRole;
	}
	
	/**
	 * Gets the author.
	 *
	 * @return the author
	 */
	public ZenodoAuthor getAuthor() {
		return author;
	}
	
	/**
	 * Gets the field author type.
	 *
	 * @return the field author type
	 */
	public ListBox getField_author_type() {
		return field_author_type;
	}
	
	
	/**
	 * Gets the author type value.
	 *
	 * @return the author type value
	 */
	public String getAuthorTypeValue(){
		
		String authorTypeSelected = null;
		if(controls_author_type.isVisible()) {
			authorTypeSelected = field_author_type.getValue();
		}
		return authorTypeSelected;
		
	}

}
