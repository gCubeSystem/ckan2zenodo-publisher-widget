package org.gcube.portlets.widgets.ckan2zenodopublisher.client.view;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.gcube.portlets.widgets.ckan2zenodopublisher.client.ui.BasicTabPanel;
import org.gcube.portlets.widgets.ckan2zenodopublisher.client.ui.basicinformation.BasicInformationView;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.SerializableEnum;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoContributor;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoCreator;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoFile;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoItem;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoMetadata;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.shared.HandlerManager;

/**
 * The Class Ckan2ZenodoViewManager.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Dec 19, 2019
 */
public class Ckan2ZenodoViewManager {

	private BasicTabPanel basicTabPanel;

	public final static HandlerManager eventBus = new HandlerManager(null);

	private List<FormValidator> forms = new ArrayList<FormValidator>();
	private BasicInformationView basicForm;
	private PublishFileViewManager publishFileVM;

	private ZenodoItem zenodoItem;

	/**
	 * Instantiates a new ckan 2 zenodo view manager.
	 */
	public Ckan2ZenodoViewManager() {
	}

	/**
	 * View for publishing.
	 *
	 * @param zenodoItem the zenodo item
	 * @return the basic tab panel
	 */
	public BasicTabPanel viewForPublishing(final ZenodoItem zenodoItem) {
		this.zenodoItem = zenodoItem;
		basicTabPanel = new BasicTabPanel();

		boolean isUpdate = false;
		if (zenodoItem.getMetadata() != null) {
			isUpdate = zenodoItem.getMetadata().getDoi() != null || zenodoItem.getDoi() != null? true : false;
		}
		// Basic Information
		int tabIndex = 0;
		basicForm = new BasicInformationView(zenodoItem, isUpdate, tabIndex);
		basicTabPanel.getAcc_basic_info().add(basicForm);
		forms.add(basicForm);

		// Files
		if (zenodoItem.getFiles() != null && zenodoItem.getFiles().size() > 0) {
			tabIndex = 1;
			publishFileVM = new PublishFileViewManager(zenodoItem.getFiles(), tabIndex);
			basicTabPanel.getAcc_files().add(publishFileVM.getView());
		} else {
			GWT.log("Hiding add files tab");
			Scheduler.get().scheduleDeferred(new ScheduledCommand() {

				@Override
				public void execute() {
					basicTabPanel.enableAddFileTab(false);
				}

			});
		}

		return basicTabPanel;

		// return basePanel;
	}

	/**
	 * Gets the list forms.
	 *
	 * @return the list forms
	 */
	public List<FormValidator> getListForms() {
		return forms;
	}

	/**
	 * Gets the list file to publish.
	 *
	 * @return the list file to publish
	 */
	public List<ZenodoFile> getListFileToPublish() {
		if(publishFileVM!=null) {
			return publishFileVM.getSelectedFileToZenodoPublishing();
		}
		return null; //Is null if any resource was attached to dataset
	}

	/**
	 * Gets the zenodo item from form.
	 *
	 * @return the zenodo item from form
	 */
	public ZenodoItem getZenodoItemFromFieldsForm() {

		// Updating Basic Information
		zenodoItem.setTitle(basicForm.getField_title().getValue());
		zenodoItem.setDoi(basicForm.getField_doi().getValue());

		// Updating Metadata
		ZenodoMetadata meta = zenodoItem.getMetadata();
		meta.setDescription(basicForm.getField_description().getValue());
		meta.setKeywords(basicForm.getTags()); // these are the keywords
		
		//Setting publication date fxing #26166
		meta.setPublication_date(basicForm.getField_publication_date().getValue());
		
		List<ZenodoCreator> creators = basicForm.getListOfCreators();
		GWT.log("Read creators from FORM: "+creators);
		meta.setCreators(creators);
		
		List<ZenodoContributor> contributors = basicForm.getListOfContributors();
		GWT.log("Read contributors from FORM: "+contributors);
		meta.setContributors(contributors);

		// upload type
		String uploadType = basicForm.getField_upload_type().getSelectedValue();
		if(uploadType!=null)
			meta.setUpload_type(new SerializableEnum<>(Arrays.asList(uploadType), meta.getUpload_type().getSelectableValues()));
		
		// publication type
		if(basicForm.getCg_publication_type().isVisible()) {
			//sets the publication_type only if the field is visible
			String publicationType = basicForm.getField_publication_type().getSelectedValue();
			if(publicationType!=null)
				meta.setPublication_type(new SerializableEnum<>(Arrays.asList(publicationType), meta.getPublication_type().getSelectableValues()));
		}
		
		//image type
		if(basicForm.getCg_image_type().isVisible()) {
			//sets the image_type only if the field is visible
			String imageType = basicForm.getField_image_type().getSelectedValue();
			if(imageType!=null) {
				meta.setImage_type(new SerializableEnum<>(Arrays.asList(imageType), meta.getImage_type().getSelectableValues()));
			}
		}
		
		// access right
		String accessRight = basicForm.getField_access_right().getSelectedValue();
		if(accessRight!=null)
			meta.setAccess_right(new SerializableEnum<>(Arrays.asList(accessRight), meta.getAccess_right().getSelectableValues()));
		
		//license
		String licenseId = basicForm.getField_license().getSelectedValue();
		if(licenseId!=null) {
			//LicenseDTO licenseBean = new LicenseDTO(licenseId, null, null);
			List<String> licenses = new ArrayList<String>();
			licenses.add(licenseId);
			meta.setLicenseIDs(licenses);
			//meta.setLicenses(Arrays.asList(licenseBean));
		}
	
		//embargo date
		Date embargoDate = basicForm.getEmbargoDate();
		if(embargoDate!=null)
			meta.setEmbargo_date(embargoDate);
		
		// access condition
		String accessCondition = basicForm.getField_access_conditions().getValue();
		if(accessCondition!=null)
			meta.setAccess_conditions(accessCondition);
		
		zenodoItem.setMetadata(meta);

		// Updating list of file for publishing
		List<ZenodoFile> publishingFile = getListFileToPublish();
		zenodoItem.setFiles(publishingFile);

		return zenodoItem;

	}

}
