package org.gcube.portlets.widgets.ckan2zenodopublisher.server.converter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.gcube.data.publishing.ckan2zenodo.model.CkanResource;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.Community;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.Contributor;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.Contributor.Type;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.Creator;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.DateInterval;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.DepositionMetadata;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.DepositionMetadata.AccessRights;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.DepositionMetadata.ImageType;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.DepositionMetadata.PublicationType;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.DepositionMetadata.UploadType;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.FileDeposition;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.RelatedIdentifier;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.RelatedIdentifier.Relation;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.Subject;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.ZenodoDeposition;
import org.gcube.portlets.widgets.ckan2zenodopublisher.server.CkanToZenodoUtil;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.SerializableEnum;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoAuthor;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoCommunity;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoContributor;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoCreator;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoDateInterval;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoFile;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoItem;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoMetadata;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoRelatedIdentifier;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoSubject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class ItemToZenodoConverter.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Dec 10, 2019
 */
public class ItemToZenodoConverter {

	private static Logger LOG = LoggerFactory.getLogger(ItemToZenodoConverter.class);

	/**
	 * **
	 * 
	 * 
	 * TO CLIENT.
	 *
	 * @param zenodoDeposition the zenodo deposition
	 * @return the zenodo item
	 */

	/**
	 * To zenodo item.
	 *
	 * @param zenodoDeposition the zenodo deposition
	 * @return the zenodo item
	 */
	public static ZenodoItem toZenodoItem(ZenodoDeposition zenodoDeposition) {

		if (zenodoDeposition == null)
			return null;

		LOG.debug("Converting item: " + zenodoDeposition);

		ZenodoItem zi = new ZenodoItem();
		zi.setId(zenodoDeposition.getId());
		zi.setDoi(zenodoDeposition.getDoi());
		zi.setCreated(zenodoDeposition.getCreated());

		// Add files already published in the deposition
		List<ZenodoFile> files = toZenodoFiles(zenodoDeposition.getFiles(), true);
		zi.addFiles(files);

		zi.setModified(zenodoDeposition.getModified());
		zi.setOwner(zenodoDeposition.getOwner());
		zi.setRecord_id(zenodoDeposition.getRecord_id());
		zi.setRecord_url(zenodoDeposition.getRecord_url() != null ? zenodoDeposition.getRecord_url().toString() : null);
		zi.setState(zenodoDeposition.getState());
		zi.setSubmitted(zenodoDeposition.getSubmitted());
		zi.setTitle(zenodoDeposition.getTitle());

		zi.setMetadata(toZenodoMetadata(zenodoDeposition.getMetadata()));

		LOG.debug("Converted item: " + zi);

		return zi;
	}

	/**
	 * Adds the files.
	 *
	 * @param files                 the files
	 * @param filesAlreadyPublished the files already published
	 * @return the list
	 */
	private static List<ZenodoFile> toZenodoFiles(ArrayList<FileDeposition> files, boolean filesAlreadyPublished) {

		if (files == null)
			return null;

		List<ZenodoFile> listOfFiles = new ArrayList<ZenodoFile>(files.size());
		for (FileDeposition fileDeposition : files) {
			listOfFiles.add(toZenodoFile(fileDeposition, filesAlreadyPublished));
		}

		return listOfFiles;
	}

	/**
	 * To zenodo files.
	 *
	 * @param filteredResources     the filtered resources
	 * @param filesAlreadyPublished the files already published
	 * @return the list
	 */
	public static List<ZenodoFile> toZenodoFiles(List<CkanResource> filteredResources, boolean filesAlreadyPublished) {
		if (filteredResources == null)
			return null;

		List<ZenodoFile> listOfFiles = new ArrayList<ZenodoFile>(filteredResources.size());
		for (CkanResource ckanResource : filteredResources) {
			listOfFiles.add(toZenodoFile(ckanResource, filesAlreadyPublished));
		}

		return listOfFiles;
	}

	/**
	 * To zenodo file.
	 *
	 * @param fileDeposition       the file deposition
	 * @param fileAlreadyPublished the file already published
	 * @return the zenodo file
	 */
	public static ZenodoFile toZenodoFile(FileDeposition fileDeposition, boolean fileAlreadyPublished) {

		if (fileDeposition == null)
			return null;

		ZenodoFile zf = new ZenodoFile();
		zf.setId(fileDeposition.getId());
		zf.setFilename(fileDeposition.getFilename());
		zf.setFilesize(fileDeposition.getFilesize() + "");
		zf.setChecksum(fileDeposition.getChecksum());
		zf.setIsAlreadyPublished(fileAlreadyPublished);
		return zf;
	}

	/**
	 * To zenodo file.
	 *
	 * @param ckanResource         the ckan resource
	 * @param fileAlreadyPublished the file already published
	 * @return the zenodo file
	 */
	public static ZenodoFile toZenodoFile(CkanResource ckanResource, boolean fileAlreadyPublished) {

		if (ckanResource == null)
			return null;

		ZenodoFile zf = new ZenodoFile();
		zf.setId(ckanResource.getId());
		zf.setFilename(ckanResource.getName());
		zf.setFilesize(ckanResource.getSize());
		zf.setChecksum(null);
		zf.setMimeType(ckanResource.getMimetype());
		zf.setFormat(ckanResource.getFormat());
		zf.setDescription(ckanResource.getDescription());
		zf.setIsAlreadyPublished(fileAlreadyPublished);
		return zf;
	}

	/**
	 * To zenodo metadata.
	 *
	 * @param depositionMetadata the deposition metadata
	 * @return the zenodo metadata
	 */
	public static ZenodoMetadata toZenodoMetadata(DepositionMetadata depositionMetadata) {

		if (depositionMetadata == null)
			return null;

		ZenodoMetadata zm = new ZenodoMetadata();
		zm.setAccess_conditions(depositionMetadata.getAccess_conditions());
		zm.setDescription(depositionMetadata.getDescription());

		// fixing bug #26165 on publication_date = null
		zm.setPublication_date(depositionMetadata.getPublication_date());

		// Access Type
		AccessRights[] accessRight = null;
		if (depositionMetadata.getAccess_right() != null) {
			AccessRights[] theAccessRight = { depositionMetadata.getAccess_right() };
			accessRight = theAccessRight;
		}
		zm.setAccess_right(CkanToZenodoUtil.toSerializableEnum(accessRight, AccessRights.values()));

		// Upload Type
		UploadType[] uploadType = null;
		if (depositionMetadata.getUpload_type() != null) {
			UploadType[] theUploadType = { depositionMetadata.getUpload_type() };
			uploadType = theUploadType;

		}
		zm.setUpload_type(CkanToZenodoUtil.toSerializableEnum(uploadType, UploadType.values()));

		// Publication Type
		PublicationType[] publicationType = null;
		if (depositionMetadata.getPublication_type() != null) {
			PublicationType[] thepublicationType = { depositionMetadata.getPublication_type() };
			publicationType = thepublicationType;
		}
		zm.setPublication_type(CkanToZenodoUtil.toSerializableEnum(publicationType, PublicationType.values()));

		// Image Type
		ImageType[] imageType = null;
		if (depositionMetadata.getImage_type() != null) {
			ImageType[] theImageType = { depositionMetadata.getImage_type() };
			imageType = theImageType;
		}
		zm.setImage_type(CkanToZenodoUtil.toSerializableEnum(imageType, ImageType.values()));

		// Licenses
		List<String> selectedLicenses = null;
		if (depositionMetadata.getLicense() != null) {
			selectedLicenses = new ArrayList<String>();
			selectedLicenses.add(depositionMetadata.getLicense()); // NEED TO ADD ALL LICENSES
		}
		if (selectedLicenses != null) {
			zm.setLicenseIDs(selectedLicenses);
		}

		if (depositionMetadata.getEmbargo_date() != null) {

		}

		zm.setKeywords(depositionMetadata.getKeywords());
		zm.setCommunities(toZenodoCommunities(depositionMetadata.getCommunities()));
		zm.setConference_acronym(depositionMetadata.getConference_acronym());
		zm.setConference_dates(depositionMetadata.getConference_dates());
		zm.setConference_place(depositionMetadata.getConference_place());
		zm.setConference_session(depositionMetadata.getConference_session());
		zm.setConference_session_part(depositionMetadata.getConference_session_part());
		zm.setConference_title(depositionMetadata.getConference_title());
		zm.setConference_url(depositionMetadata.getConference_url());

		zm.setContributors(toZenodoContributors(depositionMetadata.getContributors()));

		// to manage empty or null contributors from mapping
		zm.setContributorsTypes(CkanToZenodoUtil.toSerializableEnum(null, Contributor.Type.values()));

		zm.setCreators(toZenodoCreators(depositionMetadata.getCreators()));
		zm.setDates(toZenodoDateIntervals(depositionMetadata.getDates()));
		zm.setEmbargo_date(depositionMetadata.getEmbargo_date());
		zm.setRelated_identifiers(toRelatedIdentifiers(depositionMetadata.getRelated_identifiers()));
		zm.setSubjects(toZenodoSubjects(depositionMetadata.getSubjects()));
		zm.setThesis_supervisors(toZenodoCreators(depositionMetadata.getThesis_supervisors()));
		zm.setThesis_university(depositionMetadata.getThesis_university());
		zm.setTitle(depositionMetadata.getTitle());
		zm.setVersion(depositionMetadata.getVersion());
		zm.setNotes(depositionMetadata.getNotes());

		return zm;
	}

	/**
	 * To zenodo subjects.
	 *
	 * @param subjects the subjects
	 * @return the list
	 */
	private static List<ZenodoSubject> toZenodoSubjects(List<Subject> subjects) {
		if (subjects == null)
			return null;

		List<ZenodoSubject> list = new ArrayList<ZenodoSubject>(subjects.size());
		for (Subject sub : subjects) {
			ZenodoSubject zs = new ZenodoSubject(sub.getTerm(), sub.getIdentifier(), sub.getScheme());
			list.add(zs);
		}

		return list;
	}

	/**
	 * To related identifiers.
	 *
	 * @param related_identifiers the related identifiers
	 * @return the list
	 */
	private static List<ZenodoRelatedIdentifier> toRelatedIdentifiers(List<RelatedIdentifier> related_identifiers) {

		if (related_identifiers == null)
			return null;

		List<ZenodoRelatedIdentifier> list = new ArrayList<ZenodoRelatedIdentifier>(related_identifiers.size());
		for (RelatedIdentifier relatedIdentifier : related_identifiers) {
			Relation[] rel = { relatedIdentifier.getRelation() };
			SerializableEnum<String> sEnum = CkanToZenodoUtil.toSerializableEnum(rel, Relation.values());
			ZenodoRelatedIdentifier zdi = new ZenodoRelatedIdentifier(relatedIdentifier.getIdentifier(), sEnum);
			list.add(zdi);
		}

		return list;
	}

	/**
	 * To zenodo date intervals.
	 *
	 * @param dateIntervals the date intervals
	 * @return the list
	 */
	private static List<ZenodoDateInterval> toZenodoDateIntervals(List<DateInterval> dateIntervals) {

		if (dateIntervals == null)
			return null;

		List<ZenodoDateInterval> list = new ArrayList<ZenodoDateInterval>(dateIntervals.size());
		for (DateInterval dateInterval : dateIntervals) {
			SerializableEnum<String> types = null;
			if (dateInterval.getType() != null) {
				DateInterval.Type[] rel = { dateInterval.getType() };
				types = CkanToZenodoUtil.toSerializableEnum(rel, DateInterval.Type.values());
			}

			list.add(new ZenodoDateInterval(dateInterval.getStart(), dateInterval.getEnd(), types,
					dateInterval.getDescription()));
		}

		return list;
	}

	/**
	 * To zenodo communities.
	 *
	 * @param communities the communities
	 * @return the list
	 */
	public static List<ZenodoCommunity> toZenodoCommunities(List<Community> communities) {

		if (communities == null)
			return null;

		List<ZenodoCommunity> list = new ArrayList<ZenodoCommunity>(communities.size());
		for (Community community : communities) {
			list.add(new ZenodoCommunity(community.getIdentifier()));
		}

		return list;
	}

	/**
	 * To zenodo contributors.
	 *
	 * @param contributors the contributors
	 * @return the list
	 */
	public static List<ZenodoContributor> toZenodoContributors(List<Contributor> contributors) {

		if (contributors == null)
			return null;

		List<ZenodoContributor> list = new ArrayList<ZenodoContributor>(contributors.size());
		for (Contributor contr : contributors) {
			ZenodoContributor zc = new ZenodoContributor();
			zc.setAffiliation(contr.getAffiliation());
			zc.setGnd(contr.getGnd());
			zc.setName(contr.getName());
			zc.setOrcid(contr.getOrcid());
			SerializableEnum<String> types = null;
			if (contr.getType() != null) {
				Contributor.Type[] rel = { contr.getType() };
				types = CkanToZenodoUtil.toSerializableEnum(rel, Contributor.Type.values());
			}
			zc.setType(types);
			list.add(zc);
		}

		return list;
	}

	/**
	 * To zenodo creators.
	 *
	 * @param creators the creators
	 * @return the list
	 */
	public static List<ZenodoCreator> toZenodoCreators(List<Creator> creators) {

		if (creators == null)
			return null;

		List<ZenodoCreator> list = new ArrayList<ZenodoCreator>(creators.size());
		for (Creator contr : creators) {
			ZenodoCreator zc = new ZenodoCreator();
			zc.setAffiliation(contr.getAffiliation());
			zc.setGnd(contr.getGnd());
			zc.setName(contr.getName());
			zc.setOrcid(contr.getOrcid());
			list.add(zc);
		}

		return list;
	}

	/**
	 * To service file deposition.
	 *
	 * @param zenodoFile the zenodo file
	 * @return the file deposition
	 */
	public static FileDeposition toServiceFileDeposition(ZenodoFile zenodoFile) {

		if (zenodoFile == null)
			return null;

		FileDeposition fileDep = new FileDeposition();
		fileDep.setId(zenodoFile.getId());
		fileDep.setFilename(zenodoFile.getFilename());
		try {
			fileDep.setFilesize(Integer.parseInt(zenodoFile.getFilesize()));
		} catch (Exception e) {
			// TODO: handle exception
		}
		fileDep.setChecksum(zenodoFile.getChecksum());
		return fileDep;
	}

	/**
	 * Update metadata info of deposition metadata.
	 *
	 * @param metadata           the metadata
	 * @param depositionMetadata the deposition metadata
	 * @return the deposition metadata
	 */
	public static DepositionMetadata updateMetadataInfoOfDepositionMetadata(ZenodoMetadata metadata,
			DepositionMetadata depositionMetadata) {

		// description
		String description = metadata.getDescription();
		LOG.debug("Read description: " + description);
		if (description != null) {
			depositionMetadata.setDescription(description);
		}

		// upload type
		SerializableEnum<String> uploadType = metadata.getUpload_type();
		LOG.debug("Read upload type: " + uploadType);
		if (uploadType != null && uploadType.getSelectedValues() != null) {
			String theUploadTypeValue = uploadType.getSelectedValues().get(0);
			LOG.debug("Set upload type: " + theUploadTypeValue);
			depositionMetadata.setUpload_type(UploadType.valueOf(theUploadTypeValue));
		}

		// access right
		SerializableEnum<String> accessRight = metadata.getAccess_right();
		LOG.debug("Read access type: " + accessRight);
		if (accessRight != null && accessRight.getSelectedValues() != null) {
			try {
				String theAccessRightValue = accessRight.getSelectedValues().get(0);
				LOG.debug("Set access right: " + theAccessRightValue);
				depositionMetadata.setAccess_right(AccessRights.valueOf(theAccessRightValue));
			} catch (Exception e) {
				LOG.warn("Set access right error: ", e);
			}
		}

		// publication type
		SerializableEnum<String> publicationType = metadata.getPublication_type();
		LOG.debug("Read publication type: " + publicationType);
		if (publicationType != null && publicationType.getSelectedValues() != null) {
			try {
				String thePublicationTypeValue = publicationType.getSelectedValues().get(0);
				LOG.debug("Set publication type: " + thePublicationTypeValue);
				depositionMetadata.setPublication_type(PublicationType.valueOf(thePublicationTypeValue));
			} catch (Exception e) {
				LOG.warn("Set publication type error: ", e);
			}
		}

		// image type
		SerializableEnum<String> imageType = metadata.getImage_type();
		LOG.debug("Read image type: " + imageType);
		if (imageType != null && imageType.getSelectedValues() != null) {
			try {
				String imageTypeValue = imageType.getSelectedValues().get(0);
				LOG.debug("Set image type: " + imageTypeValue);
				depositionMetadata.setImage_type(ImageType.valueOf(imageTypeValue));
			} catch (Exception e) {
				LOG.warn("Set image type error: ", e);
			}
		}

		// embargo_date
		Date embargoDate = metadata.getEmbargo_date();
		LOG.debug("Read embargo date: " + imageType);
		if (embargoDate != null) {
			try {
				LOG.debug("Set embargo date: " + embargoDate);
				depositionMetadata.setEmbargo_date(embargoDate);
			} catch (Exception e) {
				LOG.warn("Set embargo date error: ", e);
			}
		}

		// access conditions
		String accessConditions = metadata.getAccess_conditions();
		LOG.debug("Read access conditions: " + accessConditions);
		if (accessConditions != null) {
			LOG.debug("Set access conditions: " + accessConditions);
			depositionMetadata.setAccess_conditions(accessConditions);
		}

		// license
		List<String> licenses = metadata.getLicenseIDs();
		LOG.debug("Read licenses: " + licenses);
		if (licenses != null && licenses.size() > 0) {
			try {
				String lB = licenses.get(0);
				LOG.debug("Set license: " + lB);
				depositionMetadata.setLicense(lB);
			} catch (Exception e) {
				LOG.warn("Set license error: ", e);
			}
		}

		// creators
		List<Creator> creators = null;
		List<? extends ZenodoAuthor> zenodoCreators = metadata.getCreators();
		LOG.debug("Read Creators: " + zenodoCreators);
		if (zenodoCreators != null) {
			creators = toCreators(zenodoCreators);
		}
		LOG.debug("Set Creators: " + creators);
		depositionMetadata.setCreators(creators);

		// contributors
		List<Contributor> contributors = null;
		List<? extends ZenodoAuthor> zenodoContributors = metadata.getContributors();
		LOG.debug("Read Contributors: " + zenodoContributors);
		if (zenodoContributors != null) {
			contributors = toContributors(zenodoContributors);
		}

		if (LOG.isDebugEnabled()) {
			for (Contributor contributor : contributors) {
				LOG.debug("Set contributor: name " + contributor.getName() + " type " + contributor.getType());
			}
		}
		depositionMetadata.setContributors(contributors);

		// keywords
		depositionMetadata.setKeywords(metadata.getKeywords());

		return depositionMetadata;
	}

	/**
	 * To creators.
	 *
	 * @param zenodoCreators the zenodo creators
	 * @return the list
	 */
	public static List<Creator> toCreators(List<? extends ZenodoAuthor> zenodoCreators) {

		if (zenodoCreators == null)
			return null;

		List<Creator> listOfCreators = new ArrayList<>(zenodoCreators.size());
		for (ZenodoAuthor zenodoAuthor : zenodoCreators) {
			Creator creator = new Creator();
			creator.setName(zenodoAuthor.getName());
			if (CkanToZenodoUtil.isNotEmpty(zenodoAuthor.getAffiliation())) {
				creator.setAffiliation(zenodoAuthor.getAffiliation());
			}
			if (CkanToZenodoUtil.isNotEmpty(zenodoAuthor.getGnd())) {
				creator.setGnd(zenodoAuthor.getGnd());
			}
			if (CkanToZenodoUtil.isNotEmpty(zenodoAuthor.getOrcid())) {
				creator.setOrcid(zenodoAuthor.getOrcid());
			}
			listOfCreators.add(creator);
		}

		return listOfCreators;
	}

	/**
	 * To contributors.
	 *
	 * @param zenodoContributors the zenodo contributors
	 * @return the list
	 */
	public static List<Contributor> toContributors(List<? extends ZenodoAuthor> zenodoContributors) {

		if (zenodoContributors == null)
			return null;

		List<Contributor> listOfContributors = new ArrayList<Contributor>(zenodoContributors.size());
		for (ZenodoAuthor zenodoAuthor : zenodoContributors) {
			Contributor contributor = new Contributor();
			contributor.setName(zenodoAuthor.getName());
			if (CkanToZenodoUtil.isNotEmpty(zenodoAuthor.getAffiliation())) {
				contributor.setAffiliation(zenodoAuthor.getAffiliation());
			}
			if (CkanToZenodoUtil.isNotEmpty(zenodoAuthor.getGnd())) {
				contributor.setGnd(zenodoAuthor.getGnd());
			}
			if (CkanToZenodoUtil.isNotEmpty(zenodoAuthor.getOrcid())) {
				contributor.setOrcid(zenodoAuthor.getOrcid());
			}
			Type type = null;
			String theType = null;
			try {
				theType = zenodoAuthor.getType().getSelectedValues().get(0);
				type = Type.valueOf(theType);
			} catch (Exception e) {
				LOG.error("I'm not able to convert " + theType + " as value of " + Type.class.getName());
			}
			contributor.setType(type);
			listOfContributors.add(contributor);
		}

		return listOfContributors;
	}

}
