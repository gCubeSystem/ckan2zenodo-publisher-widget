package org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped;

import java.io.Serializable;

import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.SerializableEnum;


/**
 * The Class ZenodoContributor.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Jan 15, 2020
 */
public class ZenodoContributor extends ZenodoCreator implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3422470577729844766L;
	
	private SerializableEnum<String> type;
	
	public static final String USER_ROLE = "Contributor";
	
	/**
	 * Instantiates a new zenodo contributor.
	 */
	public ZenodoContributor(){}
	
	
	/**
	 * Instantiates a new zenodo contributor.
	 *
	 * @param name the name
	 * @param affiliation the affiliation
	 * @param orcid the orcid
	 * @param gnd the gnd
	 */
	public ZenodoContributor(String name, String affiliation, String orcid, String gnd) {
		super(name, affiliation, orcid, gnd);
	}
	
	

	/* (non-Javadoc)
	 * @see org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoCreator#getType()
	 */
	public SerializableEnum<String> getType() {
		return type;
	}


	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(SerializableEnum<String> type) {
		this.type = type;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ZenodoContributor [type=");
		builder.append(type);
		builder.append(", toString()=");
		builder.append(super.toString());
		builder.append("]");
		return builder.toString();
	}
}
