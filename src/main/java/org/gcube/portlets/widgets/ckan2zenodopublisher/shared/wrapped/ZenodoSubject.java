package org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped;

import java.io.Serializable;


/**
 * The Class ZenodoSubject.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Dec 10, 2019
 */
public class ZenodoSubject implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1344448838422680493L;
	private String term;
	private String identifier;
	private String scheme;
	
	/**
	 * Instantiates a new zenodo subject.
	 */
	public ZenodoSubject(){}
	
	

	/**
	 * Instantiates a new zenodo subject.
	 *
	 * @param term the term
	 * @param identifier the identifier
	 * @param scheme the scheme
	 */
	public ZenodoSubject(String term, String identifier, String scheme) {
		super();
		this.term = term;
		this.identifier = identifier;
		this.scheme = scheme;
	}



	/**
	 * Gets the term.
	 *
	 * @return the term
	 */
	public String getTerm() {
		return term;
	}

	/**
	 * Sets the term.
	 *
	 * @param term the new term
	 */
	public void setTerm(String term) {
		this.term = term;
	}

	/**
	 * Gets the identifier.
	 *
	 * @return the identifier
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * Sets the identifier.
	 *
	 * @param identifier the new identifier
	 */
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	/**
	 * Gets the scheme.
	 *
	 * @return the scheme
	 */
	public String getScheme() {
		return scheme;
	}

	/**
	 * Sets the scheme.
	 *
	 * @param scheme the new scheme
	 */
	public void setScheme(String scheme) {
		this.scheme = scheme;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ZenodoSubject [term=");
		builder.append(term);
		builder.append(", identifier=");
		builder.append(identifier);
		builder.append(", scheme=");
		builder.append(scheme);
		builder.append("]");
		return builder.toString();
	}
	
	

}
