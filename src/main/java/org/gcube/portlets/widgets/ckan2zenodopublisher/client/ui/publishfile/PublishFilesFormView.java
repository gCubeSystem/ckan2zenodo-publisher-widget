package org.gcube.portlets.widgets.ckan2zenodopublisher.client.ui.publishfile;

import java.util.ArrayList;
import java.util.List;

import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoFile;

import com.github.gwtbootstrap.client.ui.Fieldset;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;


/**
 * The Class PublishFilesFormView.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Jan 28, 2020
 */
public class PublishFilesFormView extends Composite {

	private static PublishFilesFormViewUiBinder uiBinder = GWT.create(PublishFilesFormViewUiBinder.class);

	@UiField
	Fieldset field_form_files;

	@UiField
	Fieldset field_form_files_already_published;
	
	@UiField
	VerticalPanel panelOfFilesPublished;

	private List<PublishFileView> lstPFV = new ArrayList<PublishFileView>();
	
	private List<ZenodoFile> alreadyPublished = new ArrayList<ZenodoFile>();

	/**
	 * The Interface PublishFilesFormViewUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
	 * 
	 *         Dec 17, 2019
	 */
	interface PublishFilesFormViewUiBinder extends UiBinder<Widget, PublishFilesFormView> {
	}

	/**
	 * Instantiates a new publish files form view.
	 *
	 * @param tabIndex the tab index
	 */
	public PublishFilesFormView(int tabIndex) {
		initWidget(uiBinder.createAndBindUi(this));

	}

	/**
	 * Gets the list of publishing file view.
	 *
	 * @return the list of publishing file view
	 */
	public List<PublishFileView> getListOfPublishingFileView() {
		return lstPFV;
	}

	/**
	 * Adds the file.
	 *
	 * @param file the file
	 * @return the publish file view if is a file to publish
	 */
	public PublishFileView addFile(ZenodoFile file) {

		PublishFileView pv = null;
		if (file.getIsAlreadyPublished()) {
		
			Label alert = new Label();
			alert.setText(file.getFilename());
			panelOfFilesPublished.add(alert);
			field_form_files_already_published.setVisible(true);
			alreadyPublished.add(file);
			
		}else {
			pv = new PublishFileView(file);
			pv.getField_file_name().setValue(file.getFilename());
			pv.getSwitchButton().setValue(true);
			lstPFV.add(pv);
			field_form_files.add(pv);
		}
		return pv;
	}
	
	/**
	 * Gets the already published.
	 *
	 * @return the already published
	 */
	public List<ZenodoFile> getAlreadyPublished() {
		return alreadyPublished;
	}
	
	public Fieldset getField_form_files_already_published() {
		return field_form_files_already_published;
	}

}
