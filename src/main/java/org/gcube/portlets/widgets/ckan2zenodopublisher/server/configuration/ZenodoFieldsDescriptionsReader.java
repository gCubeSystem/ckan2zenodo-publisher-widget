package org.gcube.portlets.widgets.ckan2zenodopublisher.server.configuration;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ZenodoFieldsDescriptionsReader {

	private static final String ZENODO_FIELDS_SUGGESTIONS_PROPERTIES = "zenodo_fields_suggestions.properties";

	private static Logger LOG = LoggerFactory.getLogger(ZenodoFieldsDescriptionsReader.class);

	public static Map<String, String> readProperties() {

		Map<String, String> map = null;
		
		try (InputStream in = ZenodoFieldsDescriptionsReader.class.getResourceAsStream(ZENODO_FIELDS_SUGGESTIONS_PROPERTIES)) {
			// load a properties file
			Properties prop = new Properties();
			prop.load(in);
			map = new HashMap<String, String>(prop.keySet().size());
			for (Object key : prop.keySet()) {
				String value = (String) prop.get(key);
				LOG.debug("Zenodo Field descriptor read (" + key + ", " + value +")");
				map.put((String) key, (String) prop.get(key));
			}

		} catch (IOException e) {
			LOG.error("Error occurred on reading the property file: " + ZENODO_FIELDS_SUGGESTIONS_PROPERTIES, e);
		}
		
		return map;
	}
	
}
