package org.gcube.portlets.widgets.ckan2zenodopublisher.client.ui;

import com.github.gwtbootstrap.client.ui.AccordionGroup;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class AccordionBasicPanel extends Composite {

	private static AccordionBasicPanelUiBinder uiBinder = GWT.create(AccordionBasicPanelUiBinder.class);

	interface AccordionBasicPanelUiBinder extends UiBinder<Widget, AccordionBasicPanel> {
	}

	@UiField
	AccordionGroup acc_basic_info;
	
	@UiField
	AccordionGroup acc_files;
	
	@UiField
	AccordionGroup acc_license;
	
	public AccordionBasicPanel() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	public AccordionGroup getAcc_basic_info() {
		return acc_basic_info;
	}


	public AccordionGroup getAcc_files() {
		return acc_files;
	}


	public AccordionGroup getAcc_license() {
		return acc_license;
	}

}
