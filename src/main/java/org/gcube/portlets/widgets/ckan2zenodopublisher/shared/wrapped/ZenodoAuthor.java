package org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped;

import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.SerializableEnum;

/**
 * The Interface ZenodoAuthor.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Jan 15, 2020
 */
public interface ZenodoAuthor {
	
	
	public static final String USER_ROLE = "Author";

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName();

	/**
	 * Gets the affiliation.
	 *
	 * @return the affiliation
	 */
	public String getAffiliation();

	/**
	 * Gets the orcid.
	 *
	 * @return the orcid
	 */
	public String getOrcid();

	/**
	 * Gets the gnd.
	 *
	 * @return the gnd
	 */
	public String getGnd();

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public SerializableEnum<String> getType();
	
	
	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(SerializableEnum<String> type);

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name);

	/**
	 * Sets the affiliation.
	 *
	 * @param affiliation the new affiliation
	 */
	public void setAffiliation(String affiliation);

	/**
	 * Sets the orcid.
	 *
	 * @param orcid the new orcid
	 */
	public void setOrcid(String orcid);

}
