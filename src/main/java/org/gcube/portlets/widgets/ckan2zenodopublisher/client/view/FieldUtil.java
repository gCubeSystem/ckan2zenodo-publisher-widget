package org.gcube.portlets.widgets.ckan2zenodopublisher.client.view;

import java.util.ArrayList;
import java.util.List;

import com.github.gwtbootstrap.client.ui.ControlGroup;
import com.github.gwtbootstrap.client.ui.ListBox;
import com.github.gwtbootstrap.client.ui.TextBox;
import com.github.gwtbootstrap.client.ui.constants.ControlGroupType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class FieldUtil.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Jan 13, 2020
 */
public class FieldUtil {

	/**
	 * Checks if is valid value.
	 *
	 * @param value the value
	 * @return true, if is valid value
	 */
	public static boolean isValidValue(String value) {
		return value != null && !value.isEmpty();
	}

	/**
	 * Gets the parent control group of widget.
	 *
	 * @param widget the widget
	 * @return the parent control group of widget
	 */
	public static ControlGroup getParentControlGroupOfWidget(Widget widget) {

		if (widget == null)
			return null;

		Widget parent = widget.getParent();
		if (parent != null && parent instanceof ControlGroup) {
			return (ControlGroup) parent;
		}

		return getParentControlGroupOfWidget(parent);
	}

	/**
	 * Sets the control group.
	 *
	 * @param controlGroup the control group
	 * @param type the type
	 */
	public static void setControlGroup(ControlGroup controlGroup, ControlGroupType type) {
		try {
			if (controlGroup != null)
				controlGroup.setType(type);

		} catch (Exception e) {
			GWT.log("Error: " + e.getMessage());
		}
	}

	
	/**
	 * Gets the text value.
	 *
	 * @param box the box
	 * @return the text value
	 */
	public static String getTextValue(TextBox box) {

		return box.getText();
	}

	
	/**
	 * Adds the values to list box.
	 *
	 * @param list the list
	 * @param listValues the list values
	 */
	public static void addValuesToListBox(ListBox list, List<String> listValues) {
		if (listValues == null)
			return;

		for (int i = 0; i < listValues.size(); i++) {
			list.addItem(listValues.get(i), listValues.get(i));
		}
	}
	

	/**
	 * To zenodo ids.
	 * TODO Must passed an object License
	 *
	 * @param listLicenses the list licenses
	 * @return the list
	 */
	public static List<String> toZenodoIds(List<String> listLicenses){
		if(listLicenses==null)
			return null;
		
		List<String> licenses = new ArrayList<String>(listLicenses.size());
		for (String zenodoLicense : listLicenses) {
			licenses.add(zenodoLicense);
		}
		
		return licenses;
	}

	
	/**
	 * Select value to list box.
	 *
	 * @param list the list
	 * @param values the values
	 */
	public static void selectValueToListBox(ListBox list, List<String> values) {
		GWT.log("Selecting value: "+values);
		String selectValue = null;
		if (values == null || values.isEmpty()) {
			selectValue = null;
		} else {
			selectValue = values.get(0);
		}

		try {
			if (list.getItemCount() > 0)
				list.setSelectedValue(selectValue);
		} catch (Exception e) {
			GWT.log("error: " + e);
		}
	}

}
