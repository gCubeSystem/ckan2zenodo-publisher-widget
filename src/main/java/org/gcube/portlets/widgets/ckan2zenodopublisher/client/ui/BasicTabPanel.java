package org.gcube.portlets.widgets.ckan2zenodopublisher.client.ui;

import java.util.ArrayList;
import java.util.List;

import com.github.gwtbootstrap.client.ui.Pager;
import com.github.gwtbootstrap.client.ui.Tab;
import com.github.gwtbootstrap.client.ui.TabPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class BasicTabPanel extends Composite {

	private static BasicTabPanelUiBinder uiBinder = GWT.create(BasicTabPanelUiBinder.class);

	interface BasicTabPanelUiBinder extends UiBinder<Widget, BasicTabPanel> {
	}
	
	//public abstract void checkTabVisited();

	@UiField
	Tab acc_basic_info;

	@UiField
	Tab acc_files;
	
	@UiField
	TabPanel tabPanel;
	
	@UiField
	Pager pager;

	private List<Boolean> listActivatedTab = new ArrayList<>();
	
	public static enum PAGER {BACK, NEXT}

	public BasicTabPanel() {
		initWidget(uiBinder.createAndBindUi(this));

		listActivatedTab = new ArrayList<>();
		listActivatedTab.add(acc_basic_info.isActive());
		listActivatedTab.add(acc_files.isActive());
		
		pager.getRight().addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				managePager(PAGER.NEXT);
				
			}
		});
		
		pager.getLeft().addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				managePager(PAGER.BACK);
			}
		});
		
        Scheduler.get().scheduleDeferred(new ScheduledCommand() {
            
            @Override
            public void execute() {
            	managePager(null);
            }
        });
        
        
        class ShownEventHandler implements com.github.gwtbootstrap.client.ui.TabPanel.ShownEvent.Handler{

			@Override
			public void onShow(com.github.gwtbootstrap.client.ui.TabPanel.ShownEvent shownEvent) {
				GWT.log("tab shownEvent fired");
				managePager(null);
			}
        }
        
        ShownEventHandler handler = new ShownEventHandler();
        
//        com.github.gwtbootstrap.client.ui.TabPanel.ShowEvent.Handler myHadler = new com.github.gwtbootstrap.client.ui.TabPanel.ShowEvent.Handler() {
//			
//			@Override
//			public void onShow(com.github.gwtbootstrap.client.ui.TabPanel.ShowEvent showEvent) {
//				
//				GWT.log("showEvent fired");
//			}
//		};

		tabPanel.addShownHandler(handler);
	}

	public Tab getAcc_basic_info() {
		return acc_basic_info;
	}

	public Tab getAcc_files() {
		return acc_files;
	}


	public TabPanel getTabPanel() {
		return tabPanel;
	}
	
	public void managePager(PAGER direction){
		int selectTab = tabPanel.getSelectedTab();
		GWT.log("selectTab tab: "+selectTab);
		GWT.log("tabPanel.getWidgetCount(): "+tabPanel.getWidgetCount());
		int totatTab = tabPanel.getWidgetCount()-1;

		checkPagerVisibility(selectTab);
		
		if(direction!=null) {
			int newSelectTabIndex = direction.equals(PAGER.BACK)?selectTab-1:selectTab+1;
			//Checking boundary
			if(newSelectTabIndex<=0)
				newSelectTabIndex = 0;
			if(newSelectTabIndex>totatTab)
				newSelectTabIndex = totatTab;
			
			GWT.log("newSelectTabIndex: "+newSelectTabIndex);
			tabPanel.selectTab(newSelectTabIndex);
			checkPagerVisibility(newSelectTabIndex);
		}
	}
	
	private void checkPagerVisibility(int index) {
		pager.getLeft().setVisible(true);
		pager.getRight().setVisible(true);
		
		//first tab
		if(index<=0) {
			pager.getLeft().setVisible(false);
		}
		//last tab
		if(index>=tabPanel.getWidgetCount()-1) {
			pager.getRight().setVisible(false);
		}
	}
	
	
	public void enableAddFileTab(boolean bool) {
		//acc_files.setActive(bool);
		acc_files.setEnabled(bool);
		pager.getRight().setVisible(bool);
	}
}
