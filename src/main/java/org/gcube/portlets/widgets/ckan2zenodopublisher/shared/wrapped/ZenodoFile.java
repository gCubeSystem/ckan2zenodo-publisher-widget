package org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped;

import java.io.Serializable;


/**
 * The Class ZenodoFile.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Dec 9, 2019
 */
public class ZenodoFile implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 761497963654566281L;
	private String id;
	private String filename;
	private String description;
	private String filesize;
	private String checksum;
	private String mimeType;
	private String format;
	
	
	private Boolean isAlreadyPublished = false; //Used by Portlet to check if the file is already published on Zenodo
	
	public ZenodoFile(){}

	public ZenodoFile(String id, String filename, String description, String filesize, String checksum, String mimeType,
			String format, Boolean isAlreadyPublished) {
		super();
		this.id = id;
		this.filename = filename;
		this.description = description;
		this.filesize = filesize;
		this.checksum = checksum;
		this.mimeType = mimeType;
		this.format = format;
		this.isAlreadyPublished = isAlreadyPublished;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFilesize() {
		return filesize;
	}

	public void setFilesize(String filesize) {
		this.filesize = filesize;
	}

	public String getChecksum() {
		return checksum;
	}

	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public Boolean getIsAlreadyPublished() {
		return isAlreadyPublished;
	}

	public void setIsAlreadyPublished(Boolean isAlreadyPublished) {
		this.isAlreadyPublished = isAlreadyPublished;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ZenodoFile [id=");
		builder.append(id);
		builder.append(", filename=");
		builder.append(filename);
		builder.append(", description=");
		builder.append(description);
		builder.append(", filesize=");
		builder.append(filesize);
		builder.append(", checksum=");
		builder.append(checksum);
		builder.append(", mimeType=");
		builder.append(mimeType);
		builder.append(", format=");
		builder.append(format);
		builder.append(", isAlreadyPublished=");
		builder.append(isAlreadyPublished);
		builder.append("]");
		return builder.toString();
	}

}
