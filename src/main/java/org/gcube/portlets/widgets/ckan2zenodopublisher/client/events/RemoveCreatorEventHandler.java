package org.gcube.portlets.widgets.ckan2zenodopublisher.client.events;

import com.google.gwt.event.shared.EventHandler;


/**
 * The Interface RemoveCreatorEventHandler.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Dec 19, 2019
 */
public interface RemoveCreatorEventHandler extends EventHandler {
	
	/**
	 * On added resource.
	 *
	 * @param addResourceEvent the add resource event
	 */
	void onAddedResource(RemoveCreatorEvent addResourceEvent);
}
