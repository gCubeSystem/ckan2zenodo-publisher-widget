package org.gcube.portlets.widgets.ckan2zenodopublisher.shared;

/**
 * The Class ZenodoError.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Feb 7, 2020
 */
public class ZenodoError extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2023412792023697928L;

	public ZenodoError() {
	}

	public ZenodoError(String arg0) {
		super(arg0);

	}

	public ZenodoError(String arg0, Throwable cause) {
		super(arg0, cause);

	}

	public ZenodoError(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

	public ZenodoError(Throwable arg0) {
		super(arg0);
	}

	private String remoteMessage = null;

	private Integer responseHTTPCode = 0;

	/**
	 * Sets the response HTTP code.
	 *
	 * @param responseHTTPCode the new response HTTP code
	 */
	public void setResponseHTTPCode(Integer responseHTTPCode) {
		this.responseHTTPCode = responseHTTPCode;
	}

	/**
	 * Gets the response HTTP code.
	 *
	 * @return the response HTTP code
	 */
	public Integer getResponseHTTPCode() {
		return responseHTTPCode;
	}

	/**
	 * Gets the remote message.
	 *
	 * @return the remote message
	 */
	public String getRemoteMessage() {
		return remoteMessage;
	}

	/**
	 * Sets the remote message.
	 *
	 * @param remoteMessage the new remote message
	 */
	public void setRemoteMessage(String remoteMessage) {
		this.remoteMessage = remoteMessage;
	}

}
