package org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped;

import java.io.Serializable;
import java.util.Date;

import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.SerializableEnum;


/**
 * The Class ZenodoDateInterval.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Dec 10, 2019
 */
public class ZenodoDateInterval implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6553194706340699412L;

	private Date start;
	private Date end;
	private SerializableEnum<String> type;
	private String description;

	/**
	 * Instantiates a new zenodo date interval.
	 */
	public ZenodoDateInterval() {
	}

	/**
	 * Instantiates a new zenodo date interval.
	 *
	 * @param start the start
	 * @param end the end
	 * @param type the type
	 * @param description the description
	 */
	public ZenodoDateInterval(Date start, Date end, SerializableEnum<String> type, String description) {
		super();
		this.start = start;
		this.end = end;
		this.type = type;
		this.description = description;
	}

	/**
	 * Gets the start.
	 *
	 * @return the start
	 */
	public Date getStart() {
		return start;
	}

	/**
	 * Sets the start.
	 *
	 * @param start the new start
	 */
	public void setStart(Date start) {
		this.start = start;
	}

	/**
	 * Gets the end.
	 *
	 * @return the end
	 */
	public Date getEnd() {
		return end;
	}

	/**
	 * Sets the end.
	 *
	 * @param end the new end
	 */
	public void setEnd(Date end) {
		this.end = end;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public SerializableEnum<String> getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(SerializableEnum<String> type) {
		this.type = type;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ZenodoDateInterval [start=");
		builder.append(start);
		builder.append(", end=");
		builder.append(end);
		builder.append(", type=");
		builder.append(type);
		builder.append(", description=");
		builder.append(description);
		builder.append("]");
		return builder.toString();
	}

}
