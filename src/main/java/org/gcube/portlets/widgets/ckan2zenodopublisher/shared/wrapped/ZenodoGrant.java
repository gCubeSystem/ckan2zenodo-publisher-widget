package org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped;

import java.io.Serializable;


/**
 * The Class ZenodoGrant.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Dec 10, 2019
 */
public class ZenodoGrant implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6481107870594473881L;
	private String id;
	
	/**
	 * Instantiates a new zenodo grant.
	 */
	public ZenodoGrant() {
	}

	/**
	 * Instantiates a new zenodo grant.
	 *
	 * @param id the id
	 */
	public ZenodoGrant(String id) {
		super();
		this.id = id;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ZenodoGrant [id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}
	
	

}
