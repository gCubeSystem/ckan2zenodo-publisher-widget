package org.gcube.portlets.widgets.ckan2zenodopublisher.client.view;

import java.util.ArrayList;
import java.util.List;

import org.gcube.portlets.widgets.ckan2zenodopublisher.client.ui.publishfile.PublishFileView;
import org.gcube.portlets.widgets.ckan2zenodopublisher.client.ui.publishfile.PublishFilesFormView;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoFile;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Composite;

public class PublishFileViewManager {

	private List<ZenodoFile> listFiles;
	private PublishFilesFormView publishFileFormView;
	private int tabIndex;

	public PublishFileViewManager(List<ZenodoFile> listFiles, int tabIndex) {
		this.listFiles = listFiles;
		this.publishFileFormView = new PublishFilesFormView(tabIndex);
		addFilesToView();
	}

	private void addFilesToView() {

		if (listFiles != null) {
			for (ZenodoFile file : listFiles) {
				 publishFileFormView.addFile(file);
			}
			
//			if(publishFileFormView.getAlreadyPublished().size()>0) {
//				publishFileFormView.getField_form_files_already_published().add(new HTML("<hr>"));
//			}
		}
	}
	
	public Composite getView(){
		return publishFileFormView;
	}
	
	
	/**
	 * Gets the selected file to zenodo publishing.
	 *
	 * @return the selected file to zenodo publishing
	 */
	public List<ZenodoFile> getSelectedFileToZenodoPublishing(){
		
		List<PublishFileView> listOfPublFilesFormView = publishFileFormView.getListOfPublishingFileView();
		GWT.log("Checking Files to publish: "+listOfPublFilesFormView);
		
		 List<ZenodoFile> lisFileToPublish = new ArrayList<ZenodoFile>();
		for (PublishFileView publishFileView : listOfPublFilesFormView) {
			if(publishFileView.isToPublish()) {
				//THE FILE NAME SHOULD BE CHANGED BY USER, SO UPDATING IT
				ZenodoFile file = publishFileView.getFile();
				if(!publishFileView.getField_file_name().getValue().isEmpty()) {
					file.setFilename(publishFileView.getField_file_name().getValue());
				}
				lisFileToPublish.add(publishFileView.getFile());
			}
		}
		GWT.log("List of Files to publish (with publish ON): "+lisFileToPublish);
		return lisFileToPublish;
	}

}
