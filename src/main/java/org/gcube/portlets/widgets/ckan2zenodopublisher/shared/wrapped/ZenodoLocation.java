package org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped;

import java.io.Serializable;


/**
 * The Class ZenodoLocation.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Dec 10, 2019
 */
public class ZenodoLocation implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5866865983455214353L;
	private Double lat;
	private Double lon;
	private String place;
	private String description;
	
	/**
	 * Instantiates a new zenodo location.
	 */
	public ZenodoLocation(){}
	
	

	/**
	 * Instantiates a new zenodo location.
	 *
	 * @param lat the lat
	 * @param lon the lon
	 * @param place the place
	 * @param description the description
	 */
	public ZenodoLocation(Double lat, Double lon, String place, String description) {
		super();
		this.lat = lat;
		this.lon = lon;
		this.place = place;
		this.description = description;
	}



	/**
	 * Gets the lat.
	 *
	 * @return the lat
	 */
	public Double getLat() {
		return lat;
	}

	/**
	 * Sets the lat.
	 *
	 * @param lat the new lat
	 */
	public void setLat(Double lat) {
		this.lat = lat;
	}

	/**
	 * Gets the lon.
	 *
	 * @return the lon
	 */
	public Double getLon() {
		return lon;
	}

	/**
	 * Sets the lon.
	 *
	 * @param lon the new lon
	 */
	public void setLon(Double lon) {
		this.lon = lon;
	}

	/**
	 * Gets the place.
	 *
	 * @return the place
	 */
	public String getPlace() {
		return place;
	}

	/**
	 * Sets the place.
	 *
	 * @param place the new place
	 */
	public void setPlace(String place) {
		this.place = place;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ZenodoLocation [lat=");
		builder.append(lat);
		builder.append(", lon=");
		builder.append(lon);
		builder.append(", place=");
		builder.append(place);
		builder.append(", description=");
		builder.append(description);
		builder.append("]");
		return builder.toString();
	}
	
	

}
