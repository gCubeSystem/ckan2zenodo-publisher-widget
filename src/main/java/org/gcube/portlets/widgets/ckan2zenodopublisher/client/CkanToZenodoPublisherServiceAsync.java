package org.gcube.portlets.widgets.ckan2zenodopublisher.client;

import java.util.Map;

import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.CatalogueItem;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.DOI_dv;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoItem;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface CkanToZenodoPublisherServiceAsync {
    /**
     * Utility class to get the RPC Async interface from client-side code
     */
    public static final class Util 
    { 
        private static CkanToZenodoPublisherServiceAsync instance;

        public static final CkanToZenodoPublisherServiceAsync getInstance()
        {
            if ( instance == null )
            {
                instance = (CkanToZenodoPublisherServiceAsync) GWT.create( CkanToZenodoPublisherService.class );
            }
            return instance;
        }

        private Util()
        {
            // Utility class should not be instantiated
        }
    }

	void convertToZenodoItem(CatalogueItem item, AsyncCallback<ZenodoItem> callback);

	void publishOnZenodo(ZenodoItem zenodoItem, AsyncCallback<DOI_dv> callback);

	void readFieldsDescriptions(AsyncCallback<Map<String, String>> callback);

	void checkZenodoEnvironment(AsyncCallback<Boolean> callback);
    
    
}
