package org.gcube.portlets.widgets.ckan2zenodopublisher.client;


/**
 * The Class CkanToZendoPublisherWidgetConstant.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Dec 9, 2019
 */
public class CkanToZendoPublisherWidgetConstant {
	
	public static final String MSG_OF_ERROR_REFRESH_AND_TRY_AGAIN_OR_CONTACT_THE_SUPPORT = "Refresh and try again or contact the support";

}
