package org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped;

import java.io.Serializable;


/**
 * The Class DOI_dv.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Jul 28, 2023
 */
public class DOI_dv implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String doi;
	private String doi_url;

	/**
	 * Instantiates a new DO I dv.
	 */
	public DOI_dv() {
	}

	/**
	 * Instantiates a new DO I dv.
	 *
	 * @param doi the doi
	 * @param doi_url the doi url
	 */
	public DOI_dv(String doi, String doi_url) {
		super();
		this.doi = doi;
		this.doi_url = doi_url;
	}

	/**
	 * Gets the doi.
	 *
	 * @return the doi
	 */
	public String getDoi() {
		return doi;
	}

	/**
	 * Gets the doi url.
	 *
	 * @return the doi url
	 */
	public String getDoi_url() {
		return doi_url;
	}

	/**
	 * Sets the doi.
	 *
	 * @param doi the new doi
	 */
	public void setDoi(String doi) {
		this.doi = doi;
	}

	/**
	 * Sets the doi url.
	 *
	 * @param doi_url the new doi url
	 */
	public void setDoi_url(String doi_url) {
		this.doi_url = doi_url;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DOI_dv [doi=");
		builder.append(doi);
		builder.append(", doi_url=");
		builder.append(doi_url);
		builder.append("]");
		return builder.toString();
	}

}
