package org.gcube.portlets.widgets.ckan2zenodopublisher.client.ui.utils;

import java.util.List;

import org.gcube.portlets.widgets.ckan2zenodopublisher.client.CkanToZendoPublisherWidget;

import com.github.gwtbootstrap.client.ui.Icon;
import com.github.gwtbootstrap.client.ui.Popover;
import com.github.gwtbootstrap.client.ui.constants.Placement;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Style.Cursor;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.EventListener;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;


/**
 * The Class InfoTextAndLabels.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Dec 17, 2019
 */
public class InfoTextAndLabels {

	// TAGS
	public static final String TAGS_INFO_ID_POPUP = "tags-popup-panel-info";
	public static final String TAGS_INFO_CAPTION = "Tags";
	public static final String TAGS_INFO_TEXT = "Tags are meaningful information that can be associated to the "
			+ "item and by means of them it can be retrieved. A tag can contain only alphanumeric characters. "
			+ "If the tag is composed by a single word it must have a size of at least two characters."
			+ "Examples of good tags: \"This is a sample tag\", \"tagY\". Example of bad tag: \"c\"."
			+ " You must push ENTER for attaching a tag, or use the provided list of predefined tags. In the latter case"
			+ " hold down the CTRL or CMD button to select multiple options.";

	/**
	 * Prepare the popover and the gcube popup panel for information.
	 *
	 * @param popupId the popup id
	 * @param text the text
	 * @param captionText the caption text
	 * @param iconElement the icon element
	 * @param popover the popover
	 * @param focusPanel the focus panel
	 * @param popupOpenedIds the popup opened ids
	 */
	public static void preparePopupPanelAndPopover(final String popupId, final String text, final String captionText,
			Icon iconElement, Popover popover, FocusPanel focusPanel, final List<String> popupOpenedIds) {

		// prepare the popover
		popover.setText(new HTML("<p style='color:initial'>" + text + "</p>").getHTML());
		popover.setHeading(new HTML("<b>" + captionText + "</b>").getHTML());

		// set icon cursor
		iconElement.getElement().getStyle().setCursor(Cursor.HELP);

		// prepare the gcube dialog
		focusPanel.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				// Retrieve elemnt that should have this id
				DialogBox popup = null;
				try {
					Element element = DOM.getElementById(popupId);
					popup = (DialogBox) Widget.asWidgetOrNull(getWidget(element));
				} catch (Exception e) {
					GWT.log("ERROR", e);
				}

				// if it doesn't exist, create it
				if (popup == null) {

					popup = new DialogBox();
					popup.setText(captionText);
					popup.add(new HTML(text));

					popup.getElement().setId(popupId);
					popup.setModal(false);

					// add its id
					popupOpenedIds.add(popupId);

				}

				// then center and show
				popup.center();
				popup.show();

			}
		});

	}

	/**
	 * Check if an element of such type is actually a widget.
	 *
	 * @param element the element
	 * @return the widget
	 */
	public static IsWidget getWidget(Element element) {
		EventListener listener = DOM.getEventListener(element);
		// No listener attached to the element, so no widget exist for this
		// element
		if (listener == null) {
			GWT.log("Widget is NULL");
			return null;
		}
		if (listener instanceof Widget) {
			// GWT uses the widget as event listener
			GWT.log("Widget is " + listener);
			return (Widget) listener;
		}
		return null;
	}

	/**
	 * Close any dialog box opened.
	 *
	 * @param popupOpenedIds the popup opened ids
	 */
	public static void closeDialogBox(List<String> popupOpenedIds) {

		for (String popupid : popupOpenedIds) {
			DialogBox popup = null;
			try {
				Element element = DOM.getElementById(popupid);
				popup = (DialogBox) Widget.asWidgetOrNull(getWidget(element));
				popup.hide();
			} catch (Exception e) {
				GWT.log("ERROR", e);
			}
		}
	}
	
	
	/**
	 * Valid value.
	 *
	 * @param value the value
	 * @return the string
	 */
	public static String validValue(String value) {
		if(value==null || value.isEmpty())
			return "";
		
		return value;
	}
	
	
	/**
	 * Adds the tooltip for field key.
	 *
	 * @param theFieldKey the the field key
	 * @param widget the widget
	 */
	public static void addTooltipForFieldKey(String theFieldKey, Widget widget) {
		String value = CkanToZendoPublisherWidget.mapOfFieldsDescriptions.get(theFieldKey);
		if(value!=null) {
			String caption = theFieldKey.substring(0,1).toUpperCase()+theFieldKey.substring(1,theFieldKey.length());
			setupPopover(widget, value, caption);
		}
	}
	
    /**
     * Setup popover.
     *
     * @param w the w
     * @param message the message
     * @param heading the heading
     */
    public static void setupPopover(Widget w, String message, String heading) {
    	Popover popover = new Popover();
    	popover.setWidget(w);
    	popover.setHtml(true);
    	popover.setText(message);
    	popover.setHeading(heading);
    	popover.setPlacement(Placement.RIGHT);
    	popover.reconfigure();
    }
	


}
