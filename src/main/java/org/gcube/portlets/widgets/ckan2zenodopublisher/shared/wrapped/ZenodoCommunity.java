package org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped;

import java.io.Serializable;


/**
 * The Class ZenodoCommunity.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Dec 10, 2019
 */
public class ZenodoCommunity implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1179726596008909364L;
	private String identifier;
	
	/**
	 * Instantiates a new zenodo community.
	 */
	public ZenodoCommunity(){}

	/**
	 * Instantiates a new zenodo community.
	 *
	 * @param identifier the identifier
	 */
	public ZenodoCommunity(String identifier) {
		super();
		this.identifier = identifier;
	}

	/**
	 * Gets the identifier.
	 *
	 * @return the identifier
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * Sets the identifier.
	 *
	 * @param identifier the new identifier
	 */
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ZenodoCommunity [identifier=");
		builder.append(identifier);
		builder.append("]");
		return builder.toString();
	}
	
	

}
