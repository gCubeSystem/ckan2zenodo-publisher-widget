package org.gcube.portlets.widgets.ckan2zenodopublisher.shared;

import java.io.Serializable;

/**
 * The Class ItemTranslateError.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR Pisa (Italy)
 * Jul 6, 2020
 */
public class ItemTranslateError implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6350511079411561892L;

	private String error;

	private ERROR_TYPE errorType;

	/**
	 * The Enum ERROR_TYPE.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR Pisa (Italy)
	 * Jul 6, 2020
	 */
	public static enum ERROR_TYPE {
		INFO, WARN, ERROR
	}

	/**
	 * Instantiates a new zenodo translate error.
	 */
	public ItemTranslateError() {
	}

	/**
	 * Instantiates a new zenodo translate error.
	 *
	 * @param error the error
	 * @param errorType the error type
	 */
	public ItemTranslateError(String error, ERROR_TYPE errorType) {
		super();
		this.error = error;
		this.errorType = errorType;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public ERROR_TYPE getErrorType() {
		if(error==null)
			return ERROR_TYPE.ERROR;
		
		return errorType;
	}

	public void setErrorType(ERROR_TYPE errorType) {
		this.errorType = errorType;
	}

	@Override
	public String toString() {
		return "ItemTranslateError [error=" + error + ", errorType=" + errorType + "]";
	}
	
	
	
}
