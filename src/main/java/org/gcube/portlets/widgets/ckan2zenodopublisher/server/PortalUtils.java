package org.gcube.portlets.widgets.ckan2zenodopublisher.server;

import javax.servlet.http.HttpServletRequest;

import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.portal.PortalContext;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.vomanagement.usermanagement.model.GCubeUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PortalUtils {

	private static Logger LOG = LoggerFactory.getLogger(PortalUtils.class);

	/**
	 * Retrieve the current scope by using the portal manager.
	 *
	 * @param request     the request
	 * @param setInThread the set in thread
	 * @return a GcubeUser object
	 */
	public static String getCurrentContext(HttpServletRequest request, boolean setInThread) {

		if (request == null)
			throw new IllegalArgumentException("HttpServletRequest is null!");

		PortalContext pContext = PortalContext.getConfiguration();
		String context = pContext.getCurrentScope(request);
		LOG.debug("Returning context " + context);

		if (context != null && setInThread)
			ScopeProvider.instance.set(context);

		return context;
	}

	/**
	 * Retrieve the current token by using the portal manager.
	 *
	 * @param request     the request
	 * @param setInThread the set in thread
	 * @return a GcubeUser object
	 */
	public static String getCurrentToken(HttpServletRequest request, boolean setInThread) {

		if (request == null)
			throw new IllegalArgumentException("HttpServletRequest is null!");

		PortalContext pContext = PortalContext.getConfiguration();
		String token = pContext.getCurrentUserToken(getCurrentContext(request, false),
				getCurrentUser(request).getUsername());
		LOG.debug("Returning token " + token.substring(0, token.length() - 7) + "XXXXX");

		if (token != null && setInThread)
			SecurityTokenProvider.instance.set(token);

		return token;
	}

	/**
	 * Retrieve the current user by using the portal manager.
	 *
	 * @param request the request
	 * @return a GcubeUser object
	 */
	public static GCubeUser getCurrentUser(HttpServletRequest request) {

		if (request == null)
			throw new IllegalArgumentException("HttpServletRequest is null!");

		PortalContext pContext = PortalContext.getConfiguration();
		GCubeUser user = pContext.getCurrentUser(request);
		LOG.debug("Returning user " + user);
		return user;
	}

}
