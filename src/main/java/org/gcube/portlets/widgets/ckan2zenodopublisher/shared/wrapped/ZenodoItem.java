package org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.ItemTranslateError;

/**
 * The Class ZenodoItem.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR Pisa (Italy)
 * Jul 6, 2020
 */
public class ZenodoItem implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4007154127458109015L;
	
	private Date created;
	private String doi;
	private List<ZenodoFile> files;
	private Integer id;
	private ZenodoMetadata metadata;
	private Date modified;
	private Integer owner;
	private Integer record_id;
	private String record_url;
	private String state;
	private Boolean submitted;
	private String title;
	private String name; // this is the dataset name

	private ItemTranslateError translateError;

	/**
	 * Instantiates a new zenodo item.
	 */
	public ZenodoItem() {
	}

	/**
	 * Gets the metadata.
	 *
	 * @return the metadata
	 */
	public ZenodoMetadata getMetadata() {
		return metadata;
	}

	/**
	 * Sets the metadata.
	 *
	 * @param metadata the new metadata
	 */
	public void setMetadata(ZenodoMetadata metadata) {
		this.metadata = metadata;
	}

	/**
	 * Adds the files.
	 *
	 * @param files the files
	 */
	public void addFiles(List<ZenodoFile> files) {

		if (this.files == null) {
			this.files = files;
		} else {
			this.files.addAll(files);
		}
	}

	/**
	 * Sets the files.
	 *
	 * @param files the new files
	 */
	public void setFiles(List<ZenodoFile> files) {

		this.files = new ArrayList<ZenodoFile>();
		if (files != null) {
			this.files.addAll(files);
		}
	}

	/**
	 * Gets the created.
	 *
	 * @return the created
	 */
	public Date getCreated() {
		return created;
	}

	/**
	 * Sets the created.
	 *
	 * @param created the new created
	 */
	public void setCreated(Date created) {
		this.created = created;
	}

	/**
	 * Gets the doi.
	 *
	 * @return the doi
	 */
	public String getDoi() {
		return doi;
	}

	/**
	 * Sets the doi.
	 *
	 * @param doi the new doi
	 */
	public void setDoi(String doi) {
		this.doi = doi;
	}

	/**
	 * Gets the files.
	 *
	 * @return the files
	 */
	public List<ZenodoFile> getFiles() {
		return files;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the modified.
	 *
	 * @return the modified
	 */
	public Date getModified() {
		return modified;
	}

	/**
	 * Sets the modified.
	 *
	 * @param modified the new modified
	 */
	public void setModified(Date modified) {
		this.modified = modified;
	}

	/**
	 * Gets the owner.
	 *
	 * @return the owner
	 */
	public Integer getOwner() {
		return owner;
	}

	/**
	 * Sets the owner.
	 *
	 * @param owner the new owner
	 */
	public void setOwner(Integer owner) {
		this.owner = owner;
	}

	/**
	 * Gets the record id.
	 *
	 * @return the record id
	 */
	public Integer getRecord_id() {
		return record_id;
	}

	/**
	 * Sets the record id.
	 *
	 * @param record_id the new record id
	 */
	public void setRecord_id(Integer record_id) {
		this.record_id = record_id;
	}

	/**
	 * Gets the record url.
	 *
	 * @return the record url
	 */
	public String getRecord_url() {
		return record_url;
	}

	/**
	 * Sets the record url.
	 *
	 * @param record_url the new record url
	 */
	public void setRecord_url(String record_url) {
		this.record_url = record_url;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * Gets the submitted.
	 *
	 * @return the submitted
	 */
	public Boolean getSubmitted() {
		return submitted;
	}

	/**
	 * Sets the submitted.
	 *
	 * @param submitted the new submitted
	 */
	public void setSubmitted(Boolean submitted) {
		this.submitted = submitted;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the translate error.
	 *
	 * @return the translate error
	 */
	public ItemTranslateError getTranslateError() {
		return translateError;
	}

	/**
	 * Sets the translate error.
	 *
	 * @param translateError the new translate error
	 */
	public void setTranslateError(ItemTranslateError translateError) {
		this.translateError = translateError;
	}

	@Override
	public String toString() {
		return "ZenodoItem [created=" + created + ", doi=" + doi + ", files=" + files + ", id=" + id + ", metadata="
				+ metadata + ", modified=" + modified + ", owner=" + owner + ", record_id=" + record_id
				+ ", record_url=" + record_url + ", state=" + state + ", submitted=" + submitted + ", title=" + title
				+ ", name=" + name + ", translateError=" + translateError + "]";
	}

	

}
