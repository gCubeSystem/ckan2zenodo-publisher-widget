package org.gcube.portlets.widgets.ckan2zenodopublisher.client.ui.publishfile;

import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoFile;
import org.gcube.portlets.widgets.switchbutton.client.SwitchButton;

import com.github.gwtbootstrap.client.ui.Label;
import com.github.gwtbootstrap.client.ui.TextBox;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class PublishFileView.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Jan 10, 2020
 */
public class PublishFileView extends Composite {

	private static PublishFileViewUiBinder uiBinder = GWT.create(PublishFileViewUiBinder.class);

	@UiField
	SwitchButton switchButton;
	
	@UiField
	TextBox field_file_name;
	
	@UiField
	Label field_label_info;

	private ZenodoFile file;
	
	/**
	 * The Interface PublishFileViewUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
	 * 
	 * Jan 10, 2020
	 */
	interface PublishFileViewUiBinder extends UiBinder<Widget, PublishFileView> {
	}

	/**
	 * Instantiates a new publish file view.
	 *
	 * @param file the file
	 */
	public PublishFileView(ZenodoFile file) {
		initWidget(uiBinder.createAndBindUi(this));
		this.file = file;
		
		
		switchButton.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				
				field_file_name.setEnabled(event.getValue());
				if(event.getValue()) {
					field_file_name.getElement().getStyle().setOpacity(1.0);
				}else {
					field_file_name.getElement().getStyle().setOpacity(0.5);
				}
			}
		});
		
		setFieldName(file.getFilename());
	}

	/**
	 * Gets the switch button.
	 *
	 * @return the switch button
	 */
	public SwitchButton getSwitchButton() {
		return switchButton;
	}
	
	
	/**
	 * Hide switch button.
	 *
	 * @param bool the bool
	 * @param msg the msg
	 */
	public void hideSwitchButton(boolean bool, String msg) {
		if(bool) {
			switchButton.setVisible(false);
			field_file_name.setEnabled(false);
		}else {
			switchButton.setVisible(true);
			field_file_name.setEnabled(true);
		}
		
		field_label_info.setText(msg);
	}
	
	/**
	 * Gets the field file name.
	 *
	 * @return the field file name
	 */
	public TextBox getField_file_name() {
		return field_file_name;
	}

	/**
	 * Sets the switch button.
	 *
	 * @param switchButton the new switch button
	 */
	public void setSwitchButton(SwitchButton switchButton) {
		this.switchButton = switchButton;
	}

	/**
	 * Sets the field name.
	 *
	 * @param value the new field name
	 */
	public void setFieldName(String value) {
		this.field_file_name.setValue(value);
	}
	
	/**
	 * Gets the file.
	 *
	 * @return the file
	 */
	public ZenodoFile getFile() {
		return file;
	}
	
	/**
	 * Checks if is to publish.
	 *
	 * @return true, if is to publish
	 */
	public boolean isToPublish() {
		return switchButton.getValue();	
	}

}
