package org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped;

import java.io.Serializable;

import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.SerializableEnum;


/**
 * The Class ZenodoCreator.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Jan 15, 2020
 */
public class ZenodoCreator implements ZenodoAuthor, Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4357548679127123138L;
	private String name;
	private String affiliation;
	private String orcid;
	private String gnd;
	
	public static final String USER_ROLE = "Creator";

	/**
	 * Instantiates a new zenodo creator.
	 */
	public ZenodoCreator(){}
	

	/**
	 * Instantiates a new zenodo creator.
	 *
	 * @param name the name
	 * @param affiliation the affiliation
	 * @param orcid the orcid
	 * @param gnd the gnd
	 */
	public ZenodoCreator(String name, String affiliation, String orcid, String gnd) {
		super();
		this.name = name;
		this.affiliation = affiliation;
		this.orcid = orcid;
		this.gnd = gnd;
	}



	/* (non-Javadoc)
	 * @see org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoAuthor#getName()
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoAuthor#getAffiliation()
	 */
	public String getAffiliation() {
		return affiliation;
	}

	/**
	 * Sets the affiliation.
	 *
	 * @param affiliation the new affiliation
	 */
	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}

	/* (non-Javadoc)
	 * @see org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoAuthor#getOrcid()
	 */
	public String getOrcid() {
		return orcid;
	}

	/**
	 * Sets the orcid.
	 *
	 * @param orcid the new orcid
	 */
	public void setOrcid(String orcid) {
		this.orcid = orcid;
	}

	/* (non-Javadoc)
	 * @see org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoAuthor#getGnd()
	 */
	public String getGnd() {
		return gnd;
	}

	/**
	 * Sets the gnd.
	 *
	 * @param gnd the new gnd
	 */
	public void setGnd(String gnd) {
		this.gnd = gnd;
	}
	
	/* (non-Javadoc)
	 * @see org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoAuthor#getType()
	 */
	@Override
	public SerializableEnum<String> getType() {
		return null;
	}
	
	@Override
	public void setType(SerializableEnum<String> type) {
		//empty
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ZenodoCreator [name=");
		builder.append(name);
		builder.append(", affiliation=");
		builder.append(affiliation);
		builder.append(", orcid=");
		builder.append(orcid);
		builder.append(", gnd=");
		builder.append(gnd);
		builder.append("]");
		return builder.toString();
	}
	
	

}
