package org.gcube.portlets.widgets.ckan2zenodopublisher.client.ui.authors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.gcube.portlets.widgets.ckan2zenodopublisher.client.events.RemoveCreatorEvent;
import org.gcube.portlets.widgets.ckan2zenodopublisher.client.events.RemoveCreatorEventHandler;
import org.gcube.portlets.widgets.ckan2zenodopublisher.client.view.FieldUtil;
import org.gcube.portlets.widgets.ckan2zenodopublisher.client.view.FormValidator;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.SerializableEnum;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoAuthor;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoContributor;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoCreator;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.ControlGroup;
import com.github.gwtbootstrap.client.ui.constants.ControlGroupType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class CreatorsFormView.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Dec 17, 2019
 */
public class CreatorsFormView extends Composite implements FormValidator{

	private static CreatorsFormViewUiBinder uiBinder = GWT.create(CreatorsFormViewUiBinder.class);
	
	// event bus
	private HandlerManager eventBus = new HandlerManager(null);

	/**
	 * The Interface CreatorsFormViewUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
	 * 
	 * Dec 17, 2019
	 */
	interface CreatorsFormViewUiBinder extends UiBinder<Widget, CreatorsFormView> {
	}
	
	@UiField
	VerticalPanel fieldset_authors;
	
	@UiField
	Button button_add_author;
	
	private List<CreatorView> listCreatorView;

	private String userRole;

	private List<? extends ZenodoAuthor> listAuthors;
	
	private SerializableEnum<String> contributorTypeValues;

	//Used as defaultRole in case of empty listAuthors
	private Class theAuthorType;

	
	/**
	 * Instantiates a new creators form view.
	 *
	 * @param listAuthors the list authors
	 */
	public CreatorsFormView(List<? extends ZenodoAuthor> listAuthors, Class theAuthorType, SerializableEnum<String> contributorTypeValues) {
		initWidget(uiBinder.createAndBindUi(this));
		this.listAuthors = listAuthors;
		this.theAuthorType = theAuthorType;
		this.contributorTypeValues = contributorTypeValues;
		
		button_add_author.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				addEmptyCreator();
			}
		});
		
		eventBus.addHandler(RemoveCreatorEvent.TYPE, new RemoveCreatorEventHandler() {
			
			@Override
			public void onAddedResource(RemoveCreatorEvent addResourceEvent) {
				try {
					boolean removed = listCreatorView.remove(addResourceEvent.getCreatorView());
					GWT.log("removed creator: "+removed);
					fieldset_authors.remove(addResourceEvent.getCreatorView());
				}catch (Exception e) {
					// TODO: handle exception
				}
			}
		});

		addAuthorsToView();
	}

	
	/**
	 * Adds the authors to view.
	 */
	private void addAuthorsToView() {
		
		GWT.log("Adding listAuthors: "+listAuthors);
		
		if(listCreatorView==null)
			listCreatorView = new ArrayList<CreatorView>();
		
		if(listAuthors!=null && listAuthors.size()>0) {
			userRole = ZenodoAuthor.USER_ROLE;
			
			for (ZenodoAuthor creator : listAuthors) {
				GWT.log("Author class is "+creator.getClass());
				if (creator instanceof ZenodoContributor) {
					userRole = ZenodoContributor.USER_ROLE;
					//overriding contributors types as defined into mapping
					contributorTypeValues = new SerializableEnum<String>(Arrays.asList(creator.getType().getSelectableValues().get(0)), creator.getType().getSelectableValues());
				}else if(creator instanceof ZenodoCreator) {
					userRole = ZenodoCreator.USER_ROLE;
				}
				CreatorView view = new CreatorView(creator, userRole, eventBus, true);
				listCreatorView.add(view);
				fieldset_authors.add(view);
			}
			
			button_add_author.setText("Add another "+userRole);
			
			setRemovableCreators();
		}else {
			//ADDING EMPTY CREATOR
			addEmptyCreator();
		}
	}
	
	/**
	 * Gets the list creator view.
	 *
	 * @return the list creator view
	 */
	public List<CreatorView> getListCreatorView() {
		return listCreatorView;
	}
	
	/**
	 * Adds the empty creator.
	 */
	private void addEmptyCreator() {
		GWT.log("adding empty creator");
		
		if(listCreatorView==null)
			listCreatorView = new ArrayList<CreatorView>();
		
		ZenodoAuthor author = null;
		
		
		if(userRole == null) {
			if (theAuthorType.equals(ZenodoCreator.class)) {
				userRole = ZenodoCreator.USER_ROLE;
			}else if(theAuthorType.equals(ZenodoContributor.class)) {
				userRole = ZenodoContributor.USER_ROLE;
			}else
				userRole = ZenodoCreator.USER_ROLE;
			
			GWT.log("Empty list assigned the userRole: "+userRole);	
		}
		
		if(userRole.compareToIgnoreCase(ZenodoContributor.USER_ROLE)==0) {
			author = new ZenodoContributor();
			((ZenodoContributor) author).setType(contributorTypeValues);
		}else
			author = new ZenodoCreator();
			
		CreatorView view = new CreatorView(author, userRole, eventBus,false);
		listCreatorView.add(view);
		fieldset_authors.add(view);
		
		setRemovableCreators();
		
	}
	
	/**
	 * Sets the removable creators.
	 */
	private void setRemovableCreators() {
		
		for (int i=0; i<listCreatorView.size(); i++) {
			CreatorView creator = listCreatorView.get(i);
			boolean removableCreator = i==0?false:true;
			creator.setVisibleRemoveCreator(removableCreator);
		}
	}
	
	
	/**
	 * Read list of creators from view.
	 *
	 * @return the list<? extends zenodo author>
	 */
	public List<? extends ZenodoAuthor> readListOfCreatorsFromView(){
		
		List<ZenodoAuthor> listAuthors = new ArrayList<>();
		
		for (CreatorView creatorView : listCreatorView) {
			ZenodoAuthor author = creatorView.getAuthor();
			GWT.log("Read ZenodoAuthor: "+author);
			boolean isValid = FieldUtil.isValidValue(creatorView.getField_name().getValue());
			if(isValid) {
				GWT.log("The fielName is valid going to add it to list of Authors");
				author.setName(creatorView.getField_name().getValue());
				author.setAffiliation(creatorView.getField_affiliation().getValue());
				author.setOrcid(creatorView.getField_orcid().getValue());
				String typeSelected = creatorView.getAuthorTypeValue();
				if(typeSelected!=null && !typeSelected.isEmpty()) {
					author.setType(new SerializableEnum<>(Arrays.asList(typeSelected), null));
				}
				
				listAuthors.add(author);
			}else
				GWT.log("The fielName is null or empty skpping he/she from list of Authors");
		}
		
		GWT.log("returning list of authors: "+listAuthors);
		return listAuthors;
	}



	/* (non-Javadoc)
	 * @see org.gcube.portlets.widgets.ckan2zenodopublisher.client.view.FormValidator#isValidForm()
	 */
	@Override
	public boolean isValidForm() {
		return validateFormFields()==null;
	}

	/* (non-Javadoc)
	 * @see org.gcube.portlets.widgets.ckan2zenodopublisher.client.view.FormValidator#validateFormFields()
	 */
	@Override
	public String validateFormFields() {
		
		for (CreatorView creatorView : listCreatorView) {
			ControlGroup cgAuthor = creatorView.getControlGroup_Author();
			FieldUtil.setControlGroup(cgAuthor, ControlGroupType.NONE);
			
			if(creatorView.isMandatoryField()) {
				String value = FieldUtil.getTextValue(creatorView.getField_name());
				boolean isValid = FieldUtil.isValidValue(value);
				if(!isValid) {
					FieldUtil.setControlGroup(cgAuthor, ControlGroupType.ERROR);
					return "The field "+userRole+" Name is required";
				}
			}
			
			/*
			String affiliation = FieldUtil.getTextValue(creatorView.getField_affiliation());
			isValid = FieldUtil.isValidValue(affiliation);
			if(!isValid) {
				FieldUtil.setControlGroup(cgAuthor, ControlGroupType.ERROR);
				return "The field affiliation is required";
			}
			
			String orcid = FieldUtil.getTextValue(creatorView.getField_orcid());
			isValid = FieldUtil.isValidValue(orcid);
			if(!isValid) {
				FieldUtil.setControlGroup(cgAuthor, ControlGroupType.ERROR);
				return "The field ORCID is required";
			}
			*/
		}
		
		return null;
	}

}
