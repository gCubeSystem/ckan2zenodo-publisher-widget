package org.gcube.portlets.widgets.ckan2zenodopublisher.client.view;


/**
 * The Interface FormValidator.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Jan 9, 2020
 */
public interface FormValidator {

	
	/**
	 * Validate form fields.
	 *
	 * @return the error in case of field not valid. Null otherwise
	 */
	String validateFormFields();
	
	
	/**
	 * Checks if is valid form.
	 *
	 * @return true, if is valid form
	 */
	boolean isValidForm();
	
}
