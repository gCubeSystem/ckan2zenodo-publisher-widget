package org.gcube.portlets.widgets.ckan2zenodopublisher.client;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gcube.portlets.widgets.ckan2zenodopublisher.client.ui.BasicTabPanel;
import org.gcube.portlets.widgets.ckan2zenodopublisher.client.ui.BasicTabPanel.PAGER;
import org.gcube.portlets.widgets.ckan2zenodopublisher.client.view.Ckan2ZenodoViewManager;
import org.gcube.portlets.widgets.ckan2zenodopublisher.client.view.FormValidator;
import org.gcube.portlets.widgets.ckan2zenodopublisher.client.view.LoaderIcon;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.CatalogueItem;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.ZenodoError;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.DOI_dv;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoItem;

import com.github.gwtbootstrap.client.ui.Alert;
import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.Hero;
import com.github.gwtbootstrap.client.ui.Modal;
import com.github.gwtbootstrap.client.ui.ModalFooter;
import com.github.gwtbootstrap.client.ui.Paragraph;
import com.github.gwtbootstrap.client.ui.constants.AlertType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;


/**
 * The Class CkanToZendoPublisherWidget.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Jan 28, 2020
 */
public class CkanToZendoPublisherWidget {

	private CkanToZenodoPublisherServiceAsync ckanToZenodoService;

	private Button buttonPublish = new Button("Upload to Zenodo");
	private Ckan2ZenodoViewManager manager = null;
	private BasicTabPanel basicTabPanel;
	private ModalFooter modalFooter;
	public static Map<String, String> mapOfFieldsDescriptions = new HashMap<String, String>();
	private final Modal modal = new Modal(true);


	/**
	 * Instantiates a new ckan to zendo publisher widget.
	 */
	public CkanToZendoPublisherWidget() {
		ckanToZenodoService = CkanToZenodoPublisherServiceAsync.Util.getInstance();
	}


	/**
	 * Publish on zenodo.
	 *
	 * @param item the item
	 */
	public void publishOnZenodo(final CatalogueItem item) {
		
		modal.setTitle("Upload to Zenodo");
		modal.addStyleName("ckan2zenodo-modal-style");
		modal.setCloseVisible(true);
		buttonPublish.setEnabled(false);
		
		Hero hero = new Hero();
		hero.addStyleName("hero-unit-custom");
		hero.add(new Paragraph("By using this process you are transferring selected catalogue item content to the Zenodo Repository (link). "
				+ "This will create a new item in Zenodo and a link of the Zenodo item will be added to the catalogue item."));
		
		modal.add(hero);
		final LoaderIcon loader = new LoaderIcon("Loading data from service...");
		modal.add(loader);
		ckanToZenodoService.convertToZenodoItem(item, new AsyncCallback<ZenodoItem>() {

			@Override
			public void onFailure(Throwable caught) {
				loader.setVisible(false);
				modal.remove(loader);
				Alert erroLabel = new Alert();
				//erroLabel.setClose(false);
				erroLabel.setType(AlertType.ERROR);
				String message = getErrorMessage(caught);
				erroLabel.setText(message);
				showResults(Arrays.asList(erroLabel.asWidget()));
			}

			@Override
			public void onSuccess(ZenodoItem result) {
				GWT.log("Zenodo item returned is: "+result);
				loader.setVisible(false);
				modal.remove(loader);
				manager = new Ckan2ZenodoViewManager();
				basicTabPanel = manager.viewForPublishing(result);
				//In case of error on translating it shows an alert
				GWT.log("Translate Error is: "+result.getTranslateError());
				if(result.getTranslateError()!=null) {
					Alert alert = new Alert(result.getTranslateError().getError());
					alert.setClose(true);
					switch (result.getTranslateError().getErrorType()) {
					case INFO:
						alert.setType(AlertType.INFO);
						break;
					case WARN:
						alert.setType(AlertType.WARNING);		
						break;
					case ERROR:
					default:
						alert.setType(AlertType.ERROR);		
						break;
					}
					modal.add(alert);
				}
				modal.add(basicTabPanel);
				buttonPublish.setEnabled(true);
			}
		});
		
		ckanToZenodoService.readFieldsDescriptions(new AsyncCallback<Map<String,String>>() {



			@Override
			public void onFailure(Throwable caught) {
			}

			@Override
			public void onSuccess(Map<String, String> result) {
				mapOfFieldsDescriptions = result;
			}
		});
		
		buttonPublish.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
				if(manager==null)
					Window.alert("Sorry, the upload to zenodo is not available at this point");
				
				boolean allFormsAreValid = true;
				List<FormValidator> forms = manager.getListForms();
				for (FormValidator formValidator : forms) {
					boolean isValid = formValidator.isValidForm();
					if(!isValid)
						allFormsAreValid = false;
				}
								
				if(allFormsAreValid) {
					buttonPublish.setEnabled(false);
					basicTabPanel.getElement().getStyle().setOpacity(0.5);
					final LoaderIcon loaderPublishing = new LoaderIcon("Trying to upload on Zenodo, please wait...");
					modalFooter.add(loaderPublishing);
					//Submit publish on zenodo.
					ZenodoItem zenodoItem = manager.getZenodoItemFromFieldsForm();
					GWT.log("Publishing file:  "+zenodoItem.getFiles());
					ckanToZenodoService.publishOnZenodo(zenodoItem, new AsyncCallback<DOI_dv>() {

						@Override
						public void onFailure(Throwable caught) {
							buttonPublish.setEnabled(true);
							basicTabPanel.getElement().getStyle().setOpacity(1.0);
							modalFooter.remove(loaderPublishing);
							Alert erroLabel = new Alert();
							//erroLabel.setClose(false);
							erroLabel.setType(AlertType.ERROR);
							String message = getErrorMessage(caught);
							erroLabel.setText(message);
							showResults(Arrays.asList(erroLabel.asWidget()));
						}

						@Override
						public void onSuccess(DOI_dv doi_dv) {
							buttonPublish.setEnabled(false);
							basicTabPanel.getElement().getStyle().setOpacity(1.0);
							try {
								modalFooter.remove(loaderPublishing);
							}catch (Exception e) {
								// TODO: handle exception
							}
							String msg = "The item has been uploaded to Zenodo correctly";
					
							Alert infoLabel = new Alert();
							infoLabel.setClose(false);
							infoLabel.setType(AlertType.INFO);
							infoLabel.setText(msg);
							
							Modal modal = new Modal(true, true);
							modal.setCloseVisible(true);
							modal.setTitle("Item uploaded to Zenodo!");
							modal.add(infoLabel);
							if(doi_dv!=null) {
								FlowPanel dF = new FlowPanel();
								dF.add(new HTML("DOI: "+doi_dv.getDoi()));
								FlowPanel dFU = new FlowPanel();
								dFU.add(new HTML("DOI URL: <a href='"+doi_dv.getDoi_url()+"' target='_blank'>"+doi_dv.getDoi_url()+"</a>"));
								modal.add(dF);
								modal.add(dFU);
							}
							
							modal.show();
						
							//showResults(Arrays.asList(infoLabel.asWidget()));
						}
					});
				
				}else {
					basicTabPanel.managePager(PAGER.BACK); //TO SHOW THE FIELD/s MISSING
				}
			}
		});
		
		modalFooter = new ModalFooter(buttonPublish);
		modal.add(modalFooter);
		modal.show();
	}
	
	

	/**
	 * Gets the error message.
	 *
	 * @param caught the caught
	 * @return the error message
	 */
	public String getErrorMessage(Throwable caught) {
		
		String message = caught.getLocalizedMessage();
		
		if(caught instanceof ZenodoError) {
			GWT.log("Caught is instanceof "+ZenodoError.class.getName());
			try {
				//Trying to parse the JSON object to display only the message
				JSONValue value = JSONParser.parseStrict(((ZenodoError) caught).getRemoteMessage());
				GWT.log("value: "+value.toString());
				JSONObject jsonObject = value.isObject();
				if(jsonObject!=null) {
					JSONObject theErrorJSON = jsonObject;
					if(jsonObject.isArray() != null) {
						JSONArray array = (JSONArray) jsonObject.get("errors");
						//GWT.log("array: "+array.toString());
						theErrorJSON = (JSONObject) array.get(0);
					}
					
					JSONValue theMessage = theErrorJSON.get("message");
					String theErrors = null;
					try {
						JSONValue theError = theErrorJSON.get("errors");
						theErrors = theError.toString();
					}catch (Exception e) {
						//silent
					}
					String toReport =  "Message reported from Zenodo: " + theMessage.toString();
					if(theErrors!=null)
						toReport+= " Errors: "+theErrors;
					
					return toReport;
				}
			}catch (Exception e) {
				return message;
			}
		}
		
		return message;
		
	}
	
	
	/**
	 * Show results.
	 *
	 * @param listWidget the list widget
	 */
	private void showResults(List<Widget> listWidget) {
		VerticalPanel vp = new VerticalPanel();
		for (Widget widget : listWidget) {
			widget.getElement().getStyle().setMarginBottom(5.0, com.google.gwt.dom.client.Style.Unit.PX);
			vp.add(widget);
		}
		modal.insert(vp, 1);
		vp.getElement().focus();
	}
	
}
