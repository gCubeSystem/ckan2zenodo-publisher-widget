/*
 * 
 */
package org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped;

import java.io.Serializable;

import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.SerializableEnum;

/**
 * The Class ZenodoRelatedIdentifier.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Dec 10, 2019
 */
public class ZenodoRelatedIdentifier implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 884279610505770594L;
	private String identifier;

	private SerializableEnum<String> relation;

	/**
	 * Instantiates a new zenodo related identifier.
	 */
	public ZenodoRelatedIdentifier() {
	}

	/**
	 * Instantiates a new zenodo related identifier.
	 *
	 * @param identifier the identifier
	 * @param relation   the relation
	 */
	public ZenodoRelatedIdentifier(String identifier, SerializableEnum<String> relation) {
		super();
		this.identifier = identifier;
		this.relation = relation;
	}

	/**
	 * Gets the relation.
	 *
	 * @return the relation
	 */
	public SerializableEnum<String> getRelation() {
		return relation;
	}

	/**
	 * Sets the relation.
	 *
	 * @param relation the new relation
	 */
	public void setRelation(SerializableEnum<String> relation) {
		this.relation = relation;
	}

	/**
	 * Gets the identifier.
	 *
	 * @return the identifier
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * Sets the identifier.
	 *
	 * @param identifier the new identifier
	 */
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ZenodoRelatedIdentifier [identifier=");
		builder.append(identifier);
		builder.append(", relation=");
		builder.append(relation);
		builder.append("]");
		return builder.toString();
	}

}
