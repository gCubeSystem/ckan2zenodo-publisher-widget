package org.gcube.portlets.widgets.ckan2zenodopublisher.client.ui.basicinformation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.gcube.portlets.widgets.ckan2zenodopublisher.client.ui.authors.CreatorsFormView;
import org.gcube.portlets.widgets.ckan2zenodopublisher.client.ui.tags.TagsPanel;
import org.gcube.portlets.widgets.ckan2zenodopublisher.client.ui.utils.InfoTextAndLabels;
import org.gcube.portlets.widgets.ckan2zenodopublisher.client.view.FieldUtil;
import org.gcube.portlets.widgets.ckan2zenodopublisher.client.view.FormValidator;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoAuthor;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoContributor;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoCreator;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoItem;
import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped.ZenodoMetadata;

import com.github.gwtbootstrap.client.ui.Alert;
import com.github.gwtbootstrap.client.ui.ControlGroup;
import com.github.gwtbootstrap.client.ui.ControlLabel;
import com.github.gwtbootstrap.client.ui.Fieldset;
import com.github.gwtbootstrap.client.ui.ListBox;
import com.github.gwtbootstrap.client.ui.TextArea;
import com.github.gwtbootstrap.client.ui.TextBox;
import com.github.gwtbootstrap.client.ui.constants.ControlGroupType;
import com.github.gwtbootstrap.datepicker.client.ui.DateBox;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class CreateTaskConfigurationView.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it May 4, 2018
 */
public class BasicInformationView extends Composite implements FormValidator {

	/** The ui binder. */
	private static BasicInformationViewUiBinder uiBinder = GWT.create(BasicInformationViewUiBinder.class);

	private List<CreatorsFormView> listOfCreatorsView;
	private List<CreatorsFormView> listOfContributorsView;

	/**
	 * The Interface CreateTaskConfigurationViewUiBinder.
	 *
	 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it May 4, 2018
	 */
	interface BasicInformationViewUiBinder extends UiBinder<Widget, BasicInformationView> {
	}

	@UiField
	TextBox field_title;

	@UiField
	ControlLabel cl_title;

	@UiField
	TextBox field_doi;

	@UiField
	Fieldset fieldset_basic_informations;

	@UiField
	TextArea field_description;

	@UiField
	ControlLabel cl_description;

	@UiField
	ListBox field_upload_type;

	@UiField
	ListBox field_image_type;
	
	@UiField
	ControlGroup cg_access_conditions;
	
	@UiField
	TextArea field_access_conditions;
	
	@UiField
	ControlGroup cg_publication_type;

	@UiField
	ControlGroup cg_image_type;
	
	@UiField
	ControlGroup cg_embargo_date;

	@UiField
	ControlLabel cl_upload_type;

	@UiField
	ListBox field_access_right;

	@UiField
	ControlLabel cl_access_right;

	@UiField
	ListBox field_license;

	@UiField
	ControlLabel cl_license;

	@UiField
	ListBox field_publication_type;

	@UiField
	ControlLabel cl_publication_type;

	@UiField
	DateBox field_publication_date;

	@UiField
	ControlLabel cl_publication_date;

	@UiField
	Alert error_alert;

	@UiField
	TagsPanel the_tags_panel;
	
	@UiField
	DateBox embargoDate;
	
	@UiField
	ControlLabel cl_embargo_date;
	
	@UiField
	ControlLabel cl_access_conditions;

//	@UiField
//	Pager pager;

	// public final static HandlerManager eventBus = new HandlerManager(null);

	private boolean isEditConfiguration;

	private ZenodoItem zenodoItem;

	/**
	 * Because this class has a default constructor, it can be used as a binder
	 * template. In other words, it can be used in other *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder" xmlns:g=
	 * "urn:import:**user's package**">
	 * <g:**UserClassName**>Hello!</g:**UserClassName> </ui:UiBinder> Note that
	 * depending on the widget that is used, it may be necessary to implement
	 * HasHTML instead of HasText.
	 *
	 * @param zenodoItem the zenodo item
	 * @param isUpdate   the is update
	 * @param tabIndex   the tab index
	 */
	public BasicInformationView(final ZenodoItem zenodoItem, boolean isUpdate, final int tabIndex) {
		initWidget(uiBinder.createAndBindUi(this));
		this.zenodoItem = zenodoItem;
		fillForm();
		setVisibleFieldsForUpdate(isUpdate);

		field_upload_type.addChangeHandler(new ChangeHandler() {

			@Override
			public void onChange(ChangeEvent event) {
				
				cg_publication_type.setVisible(false);
				cg_image_type.setVisible(false);

				if(zenodoItem.getMetadata().getPublication_type()!=null) {
					List<String> publicationTypeSelected = zenodoItem.getMetadata().getPublication_type().getSelectedValues();
					boolean selectedFromServerModel = publicationTypeSelected!=null;
					cg_publication_type.setVisible(selectedFromServerModel);
				}
				
				String selectedLower = field_upload_type.getSelectedValue().toLowerCase();
				if (selectedLower.compareTo("publication") == 0) {
					cg_publication_type.setVisible(true);
					GWT.log("Upload type selected: publication");
				}else if(selectedLower.compareTo("image")==0) {
					GWT.log("Upload type selected: image");
					cg_image_type.setVisible(true);
				}
			}
		});
		
		field_access_right.addChangeHandler(new ChangeHandler() {
			
			@Override
			public void onChange(ChangeEvent event) {
				cg_embargo_date.setVisible(false);
				cg_access_conditions.setVisible(false);
				
				if(zenodoItem.getMetadata().getEmbargo_date()!=null) {
					Date embDate = zenodoItem.getMetadata().getEmbargo_date();
					boolean insertedFromModel = embDate!=null;
					cg_embargo_date.setVisible(insertedFromModel);
				}
				
				if(zenodoItem.getMetadata().getAccess_conditions()!=null) {
					cg_access_conditions.setVisible(true);
				}
				
				String selectedLower = field_access_right.getSelectedValue().toLowerCase();
				if(selectedLower.compareTo("open")==0 || selectedLower.compareTo("embargoed")==0) {
					cg_embargo_date.setVisible(true);
				}else if(selectedLower.compareTo("restricted")==0) {
					cg_access_conditions.setVisible(true);
				}
				
			}
		});
		
		embargoDate.setPlaceholder("Insert Embargo Date");
		embargoDate.setValue(null);
	}
	

	/**
	 * Gets the embargo date.
	 *
	 * @return the embargo date
	 */
	public Date getEmbargoDate(){
		return embargoDate.getValue();
		
	}

	/**
	 * Sets the visible fields for update.
	 *
	 * @param isUpdate the new visible fields for update
	 */
	protected void setVisibleFieldsForUpdate(boolean isUpdate) {

		List<Widget> listWidgtes = new ArrayList<Widget>();
		listWidgtes.add(field_doi);
		listWidgtes.add(field_publication_date);

		for (Widget widget : listWidgtes) {
			Widget parent = FieldUtil.getParentControlGroupOfWidget(widget); // It should be the 'ControlGroup'
			if (parent instanceof ControlGroup) {
				parent.setVisible(isUpdate);
			}
		}
	}

	/**
	 * Fill parameters to operator.
	 */
	private void fillForm() {

		field_title.setValue(zenodoItem.getTitle());
		InfoTextAndLabels.addTooltipForFieldKey("title", cl_title);
		field_description.setValue(zenodoItem.getMetadata().getDescription());
		InfoTextAndLabels.addTooltipForFieldKey("description", cl_description);
		field_doi.setValue(zenodoItem.getDoi());
		
		GWT.log("Filling form with: "+zenodoItem.getMetadata());

		ZenodoMetadata zMeta = zenodoItem.getMetadata();
		if (zMeta != null) {
			
			String title = zenodoItem.getTitle() != null ? zenodoItem.getTitle() : zMeta.getTitle();
			field_title.setValue(title); // Re.fill title to be sure
			
			if(zMeta.getPublication_date()!=null) {
				field_publication_date.setValue(zenodoItem.getMetadata().getPublication_date());
			}

			// Upload type
			if (zMeta.getUpload_type() != null) {
				FieldUtil.addValuesToListBox(field_upload_type, zMeta.getUpload_type().getSelectableValues());
				FieldUtil.selectValueToListBox(field_upload_type, zMeta.getUpload_type().getSelectedValues());
			} else
				field_upload_type.setEnabled(false);
			
			InfoTextAndLabels.addTooltipForFieldKey("upload_type", cl_upload_type);

			// Publication Type
			if (zMeta.getPublication_type() != null) {
				FieldUtil.addValuesToListBox(field_publication_type, zMeta.getPublication_type().getSelectableValues());
				if (zMeta.getPublication_type().getSelectedValues() != null) {
					FieldUtil.selectValueToListBox(field_publication_type,
							zMeta.getPublication_type().getSelectedValues());
				} else
					cg_publication_type.setVisible(false);
			} else
				cg_publication_type.setVisible(false);
			
			InfoTextAndLabels.addTooltipForFieldKey("publication_type", cl_publication_type);
			
			if(zMeta.getImage_type() != null) {
				FieldUtil.addValuesToListBox(field_image_type, zMeta.getImage_type().getSelectableValues());
				if (zMeta.getImage_type().getSelectedValues() != null) {
					FieldUtil.selectValueToListBox(field_image_type,
							zMeta.getImage_type().getSelectedValues());
				} else
					cg_image_type.setVisible(false);
			}else
				cg_image_type.setVisible(false);


			// Access Right
			if (zMeta.getAccess_right().getSelectedValues() != null) {
				FieldUtil.addValuesToListBox(field_access_right, zMeta.getAccess_right().getSelectableValues());
				FieldUtil.selectValueToListBox(field_access_right, zMeta.getAccess_right().getSelectedValues());
			} else
				field_access_right.setEnabled(false);

			InfoTextAndLabels.addTooltipForFieldKey("access_right", cl_access_right);
			
			if(zMeta.getEmbargo_date()!=null) {
				embargoDate.setValue(zMeta.getEmbargo_date());
				cg_embargo_date.setVisible(true);
			}else
				cg_embargo_date.setVisible(false);
			
			InfoTextAndLabels.addTooltipForFieldKey("embargo_date", cl_embargo_date);
			
			if(zMeta.getAccess_conditions()!=null) {
				field_access_conditions.setValue(zMeta.getAccess_conditions());
			}else
				cg_access_conditions.setVisible(false);
			
			InfoTextAndLabels.addTooltipForFieldKey("access_conditions", cl_access_conditions);

			// License
			if (zMeta.getLicenseIDs() != null) {
				//List<String> listLicensesIds = null;
				List<String> selectedLicensesId = FieldUtil.toZenodoIds(zMeta.getLicenseIDs());
				if(selectedLicensesId!=null) {
					FieldUtil.addValuesToListBox(field_license, selectedLicensesId);
					field_license.setEnabled(true); 
				}else {
					//Exception case on missing list of selectable license ids
					//FieldUtil.addValuesToListBox(field_license, selectedLicensesId);
					field_license.setEnabled(false); //only one license id is available
		
				}
				//FieldUtil.selectValueToListBox(field_license, selectedLicensesId);
			}else
				field_license.setEnabled(false); 

			InfoTextAndLabels.addTooltipForFieldKey("license", cl_license);

			// Keywords
			if (zMeta.getKeywords() != null && zMeta.getKeywords().size() > 0) {
				for (String keyword : zMeta.getKeywords()) {
					the_tags_panel.addTagElement(keyword);
				}
			}

			if (listOfCreatorsView == null)
				listOfCreatorsView = new ArrayList<CreatorsFormView>();

			if (listOfContributorsView == null)
				listOfContributorsView = new ArrayList<CreatorsFormView>();

			// ADDING AUTHORS
			try {
				List<? extends ZenodoAuthor> authors = zenodoItem.getMetadata().getCreators();
				GWT.log("Adding creators: " + authors);
				CreatorsFormView authorView = new CreatorsFormView(authors, ZenodoCreator.class, zenodoItem.getMetadata().getContributorsTypes());
				listOfCreatorsView.add(authorView);
			}catch (Exception e) {
				//silent
				e.printStackTrace();
			}

			try {
				List<? extends ZenodoAuthor> contributors = zenodoItem.getMetadata().getContributors();
				GWT.log("Adding contributors: " + contributors);
				CreatorsFormView contributorView = new CreatorsFormView(contributors, ZenodoContributor.class, zenodoItem.getMetadata().getContributorsTypes());
				listOfContributorsView.add(contributorView);
			}catch (Exception e) {
				e.printStackTrace();
			}

			for (CreatorsFormView cfv : listOfCreatorsView) {
				fieldset_basic_informations.add(cfv);
			}

			for (CreatorsFormView cfv : listOfContributorsView) {
				fieldset_basic_informations.add(cfv);
			}

		}
	}

	/**
	 * Checks if is edits the configuration.
	 *
	 * @return the isEditConfiguration
	 */
	public boolean isEditConfiguration() {

		return isEditConfiguration;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.gcube.portlets.widgets.ckan2zenodopublisher.client.view.FormValidator#
	 * validateForm()
	 */
	@Override
	public String validateFormFields() {

		error_alert.setVisible(false);
		ControlGroup cgDOI = FieldUtil.getParentControlGroupOfWidget(field_doi);
		ControlGroup cgTitle = FieldUtil.getParentControlGroupOfWidget(field_title);

		FieldUtil.setControlGroup(cgDOI, ControlGroupType.NONE);
		FieldUtil.setControlGroup(cgTitle, ControlGroupType.NONE);
		FieldUtil.setControlGroup(cg_embargo_date, ControlGroupType.NONE);
		FieldUtil.setControlGroup(cg_access_conditions, ControlGroupType.NONE);

		// Validating DOI
		if (cgDOI.isVisible()) {
			GWT.log("Checking doi---");
			String value = getTextValue(field_doi);
			boolean isValid = FieldUtil.isValidValue(value);
			if (!isValid) {
				FieldUtil.setControlGroup(cgDOI, ControlGroupType.ERROR);
				showErrorAlert(true, "The field DOI is required");
//				error_alert.setVisible(true);
//				error_alert.setText("This field DOI is required");
				return "This field is required";
			}
		}

		// Validating Title
		if (cgTitle.isVisible()) {
			GWT.log("Checking title---");
			String value = getTextValue(field_title);
			boolean isValid = FieldUtil.isValidValue(value);
			GWT.log("isValid: " + isValid);
			if (!isValid) {
				FieldUtil.setControlGroup(cgTitle, ControlGroupType.ERROR);
				showErrorAlert(true, "The field Title is required");
//				error_alert.setVisible(true);
//				error_alert.setText("The field Title is required");
				return "This field is required";
			}
		}
		
		if(cg_embargo_date.isVisible()) {
			boolean isValid = embargoDate.getValue()!=null;
			if(!isValid) {
				FieldUtil.setControlGroup(cg_embargo_date, ControlGroupType.ERROR);
				showErrorAlert(true, "The field Embargo Date is required");
//				error_alert.setVisible(true);
//				error_alert.setText("The field Embargo Date is required");
				return "This field is required";
			}
		}
		
		if(cg_access_conditions.isVisible()) {
			boolean isValid = field_access_conditions.getValue()!= null && !field_access_conditions.getValue().isEmpty();
			if(!isValid) {
				FieldUtil.setControlGroup(cg_access_conditions, ControlGroupType.ERROR);
				showErrorAlert(true, "The field Access Condition is required");
//				error_alert.setVisible(true);
//				error_alert.setText("The field Access Condition is required");
				return "This field is required";
			}
		}

		// Validating Creators
		for (CreatorsFormView cfv : listOfCreatorsView) {
			String error = cfv.validateFormFields();
			if (error != null) {
//				error_alert.setVisible(true);
//				error_alert.setText(error);
				showErrorAlert(true, error);
				return error;
			}
		}

		// Validating Contributors
		for (CreatorsFormView cfv : listOfContributorsView) {
			String error = cfv.validateFormFields();
			if (error != null) {
				showErrorAlert(true, error);
//				error_alert.setVisible(true);
//				error_alert.setText(error);
				return error;
			}
		}

		return null;
	}
	
	private void showErrorAlert(boolean visible, String message) {
		error_alert.setText(message);
		error_alert.setVisible(visible);
		try {
			error_alert.getElement().focus();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.gcube.portlets.widgets.ckan2zenodopublisher.client.view.FormValidator#
	 * isValidForm()
	 */
	@Override
	public boolean isValidForm() {
		return validateFormFields() == null;
	}

	/**
	 * Gets the text value.
	 *
	 * @param box the box
	 * @return the text value
	 */
	private String getTextValue(TextBox box) {

		return box.getText();
	}

	/**
	 * Gets the list of contributors view.
	 *
	 * @return the list of contributors view
	 */
	public List<CreatorsFormView> getListOfContributorsView() {
		return listOfContributorsView;
	}

	/**
	 * Gets the list of creators view.
	 *
	 * @return the list of creators view
	 */
	public List<CreatorsFormView> getListOfCreatorsView() {
		return listOfCreatorsView;
	}

	/**
	 * Gets the list of authors.
	 *
	 * @return the list of authors
	 */
	public List<ZenodoCreator> getListOfCreators() {
		List<ZenodoCreator> listOfCreators = new ArrayList<>();
		if(listOfCreatorsView==null) {
			return listOfCreators;
		}
		for (CreatorsFormView cfv : listOfCreatorsView) {
			List<? extends ZenodoAuthor> listOfUsers = cfv.readListOfCreatorsFromView();
			for (ZenodoAuthor zenodoAuthor : listOfUsers) {
				listOfCreators.add((ZenodoCreator) zenodoAuthor);
			}
		}
		return listOfCreators;
	}

	/**
	 * Gets the list of contributors.
	 *
	 * @return the list of contributors
	 */
	public List<ZenodoContributor> getListOfContributors() {
		List<ZenodoContributor> listOfContributors = new ArrayList<>();
		if(listOfContributorsView==null) {
			return listOfContributors;
		}
		for (CreatorsFormView cfv : listOfContributorsView) {
			List<? extends ZenodoAuthor> listOfUsers = cfv.readListOfCreatorsFromView();
			for (ZenodoAuthor zenodoAuthor : listOfUsers) {
				listOfContributors.add((ZenodoContributor) zenodoAuthor);
			}
		
		}
		return listOfContributors;
	}

	/**
	 * Gets the field title.
	 *
	 * @return the field title
	 */
	public TextBox getField_title() {
		return field_title;
	}

	/**
	 * Gets the field doi.
	 *
	 * @return the field doi
	 */
	public TextBox getField_doi() {
		return field_doi;
	}

	/**
	 * Gets the fieldset basic informations.
	 *
	 * @return the fieldset basic informations
	 */
	public Fieldset getFieldset_basic_informations() {
		return fieldset_basic_informations;
	}

	/**
	 * Gets the field description.
	 *
	 * @return the field description
	 */
	public TextArea getField_description() {
		return field_description;
	}

	/**
	 * Gets the field upload type.
	 *
	 * @return the field upload type
	 */
	public ListBox getField_upload_type() {
		return field_upload_type;
	}

	/**
	 * Gets the field access right.
	 *
	 * @return the field access right
	 */
	public ListBox getField_access_right() {
		return field_access_right;
	}
	
	/**
	 * Gets the field access conditions.
	 *
	 * @return the field access conditions
	 */
	public TextArea getField_access_conditions() {
		return field_access_conditions;
	}
	
	/**
	 * Gets the field image type.
	 *
	 * @return the field image type
	 */
	public ListBox getField_image_type() {
		return field_image_type;
	}

	/**
	 * Gets the field license.
	 *
	 * @return the field license
	 */
	public ListBox getField_license() {
		return field_license;
	}

	/**
	 * Gets the field publication type.
	 *
	 * @return the field publication type
	 */
	public ListBox getField_publication_type() {
		return field_publication_type;
	}
	
	/**
	 * Gets the cg publication type.
	 *
	 * @return the cg publication type
	 */
	public ControlGroup getCg_publication_type() {
		return cg_publication_type;
	}
	
	/**
	 * Gets the cg image type.
	 *
	 * @return the cg image type
	 */
	public ControlGroup getCg_image_type() {
		return cg_image_type;
	}

	/**
	 * Gets the field publication date.
	 *
	 * @return the field publication date
	 */
	public DateBox getField_publication_date() {
		return field_publication_date;
	}

	/**
	 * Gets the error alert.
	 *
	 * @return the error alert
	 */
	public Alert getError_alert() {
		return error_alert;
	}

	/**
	 * Gets the tags.
	 *
	 * @return the tags
	 */
	public List<String> getTags() {
		return the_tags_panel.getTags();
	}

	/**
	 * Gets the list of keywords.
	 *
	 * @return the list of keywords
	 */
	public List<String> getListOfKeywords() {
		return the_tags_panel.getTags();
	}

}
