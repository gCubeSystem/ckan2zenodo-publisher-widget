package org.gcube.portlets.widgets.ckan2zenodopublisher.shared;

import java.io.Serializable;


/**
 * The Class CatalogueItem.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Dec 9, 2019
 */
public class CatalogueItem implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String itemId;
	String itemName;
	String itemURL;
	ITEM_TYPE itemType;

	public static enum ITEM_TYPE{DATASET, RESOURCE}
	
	public CatalogueItem() {}

	public CatalogueItem(String itemId, String itemName, String itemURL, ITEM_TYPE itemType) {
		this.itemId = itemId;
		this.itemName = itemName;
		this.itemURL = itemURL;
		this.itemType = itemType;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemURL() {
		return itemURL;
	}

	public void setItemURL(String itemURL) {
		this.itemURL = itemURL;
	}

	public ITEM_TYPE getItemType() {
		return itemType;
	}

	public void setItemType(ITEM_TYPE itemType) {
		this.itemType = itemType;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CatalogueItem [itemId=");
		builder.append(itemId);
		builder.append(", itemName=");
		builder.append(itemName);
		builder.append(", itemURL=");
		builder.append(itemURL);
		builder.append(", itemType=");
		builder.append(itemType);
		builder.append("]");
		return builder.toString();
	}
	
	
}
