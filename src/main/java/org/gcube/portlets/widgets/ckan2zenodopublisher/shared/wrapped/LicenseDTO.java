//package org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped;
//
//import java.io.Serializable;
//
///**
// * A license bean like the ckan's one.
// *
// * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
// * 
// *         Jan 13, 2020
// */
//public class LicenseDTO implements Serializable {
//
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 8796338619448833841L;
//	private String id;
//	private String title;
//	private String url;
//
//	/**
//	 * Instantiates a new license bean.
//	 */
//	public LicenseDTO() {
//	}
//
//	public LicenseDTO(String id, String title, String url) {
//		this.id = id;
//		this.title = title;
//		this.url = url;
//	}
//
//	public String getId() {
//		return id;
//	}
//
//	public String getTitle() {
//		return title;
//	}
//
//	public String getUrl() {
//		return url;
//	}
//
//	public void setId(String id) {
//		this.id = id;
//	}
//
//	public void setTitle(String title) {
//		this.title = title;
//	}
//
//	public void setUrl(String url) {
//		this.url = url;
//	}
//
//	@Override
//	public String toString() {
//		StringBuilder builder = new StringBuilder();
//		builder.append("LicenseDTO [id=");
//		builder.append(id);
//		builder.append(", title=");
//		builder.append(title);
//		builder.append(", url=");
//		builder.append(url);
//		builder.append("]");
//		return builder.toString();
//	}
//
//}
