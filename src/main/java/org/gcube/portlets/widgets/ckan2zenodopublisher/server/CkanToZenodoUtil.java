package org.gcube.portlets.widgets.ckan2zenodopublisher.server;

import java.util.ArrayList;
import java.util.List;

import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.SerializableEnum;


/**
 * The Class CkanToZenodoUtil.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 * Dec 10, 2019
 */
public class CkanToZenodoUtil {

	/**
	 * Gets the names.
	 *
	 * @param arrayEnum the list values
	 * @return the names
	 */
	public static List<String> getNames(Enum<?>[] arrayEnum) {

		List<String> list = new ArrayList<>();
		for (Enum<?> s : arrayEnum) {
			list.add(s.name());
		}

		return list;
	}

	
	/**
	 * To serializable enum.
	 *
	 * @param <T> the generic type
	 * @param enumSelected the enum selected
	 * @param enumValues the enum values
	 * @return list of Values and list of selected values read from input Enum 
	 */
	public static <T extends Enum<T>> SerializableEnum<String> toSerializableEnum(T[] enumSelected, T[] enumValues) {

		List<String> selectedValues = enumSelected!=null?CkanToZenodoUtil.getNames(enumSelected):null;
		List<String> values = CkanToZenodoUtil.getNames(enumValues);
		return new SerializableEnum<String>(selectedValues, values);
	}
	
	/**
	 * Checks if is not empty.
	 *
	 * @param value the value
	 * @return true, if is not empty
	 */
	public static boolean isNotEmpty(String value) {
		if(value!=null && !value.isEmpty())
			return true;
		return false;
	}
}
