package org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.gcube.portlets.widgets.ckan2zenodopublisher.shared.SerializableEnum;

/**
 * The Class ZenodoMetadata.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
 * 
 *         Jan 20, 2020
 */
public class ZenodoMetadata implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1885238190029199362L;

	private SerializableEnum<String> upload_type;

	private SerializableEnum<String> publication_type;

	private SerializableEnum<String> image_type;

	// to manage empty list or null contributors
	private SerializableEnum<String> contributorsTypes;

	private Date publication_date;
	private String title;

	private List<ZenodoCreator> creators;
	private List<ZenodoContributor> contributors;
	private List<ZenodoCreator> thesis_supervisors;

	private String description; // TODO HTML

	private SerializableEnum<String> access_right;

	private Date embargo_date;
	private String access_conditions; // TODO HTML
	private String doi;
	private Boolean preserve_doi;
	private List<String> keywords;
	private String notes; // TODO HTML

	private List<ZenodoRelatedIdentifier> related_identifiers;

	private List<String> references;
	private List<ZenodoCommunity> communities;
	private List<ZenodoGrant> grants;
	private String journal_title;
	private String journal_volume;
	private String journal_issue;
	private String journal_pages;
	private String conference_title;
	private String conference_acronym;
	private String conference_dates;
	private String conference_place;
	private String conference_url;
	private String conference_session;
	private String conference_session_part;
	private String imprint_publisher;
	private String imprint_isbn;
	private String imprint_place;
	private String partof_title;
	private String partof_pages;

	private String thesis_university;
	private List<ZenodoSubject> subjects;
	private String version;
	private String language; // https://www.loc.gov/standards/iso639-2/php/code_list.php
	private List<ZenodoLocation> locations;
	private List<ZenodoDateInterval> dates;
	private String method; // TODO html

	// private SerializableEnum<String> licenseIDs;

	private List<String> licenseIDs;

	/**
	 * Instantiates a new zenodo metadata.
	 */
	public ZenodoMetadata() {
	}

	/**
	 * Gets the upload type.
	 *
	 * @return the upload type
	 */
	public SerializableEnum<String> getUpload_type() {
		return upload_type;
	}

	/**
	 * Sets the upload type.
	 *
	 * @param upload_type the new upload type
	 */
	public void setUpload_type(SerializableEnum<String> upload_type) {
		this.upload_type = upload_type;
	}

	/**
	 * Gets the contributors types.
	 *
	 * @return the contributors types
	 */
	public SerializableEnum<String> getContributorsTypes() {
		return contributorsTypes;
	}

	/**
	 * Sets the contributors types.
	 *
	 * @param contributorsTypes the new contributors types
	 */
	public void setContributorsTypes(SerializableEnum<String> contributorsTypes) {
		this.contributorsTypes = contributorsTypes;
	}

	/**
	 * Gets the publication type.
	 *
	 * @return the publication type
	 */
	public SerializableEnum<String> getPublication_type() {
		return publication_type;
	}

	/**
	 * Sets the publication type.
	 *
	 * @param publication_type the new publication type
	 */
	public void setPublication_type(SerializableEnum<String> publication_type) {
		this.publication_type = publication_type;
	}

	/**
	 * Gets the image type.
	 *
	 * @return the image type
	 */
	public SerializableEnum<String> getImage_type() {
		return image_type;
	}

	/**
	 * Sets the image type.
	 *
	 * @param image_type the new image type
	 */
	public void setImage_type(SerializableEnum<String> image_type) {
		this.image_type = image_type;
	}

	/**
	 * Gets the publication date.
	 *
	 * @return the publication date
	 */
	public Date getPublication_date() {
		return publication_date;
	}

	/**
	 * Sets the publication date.
	 *
	 * @param publication_date the new publication date
	 */
	public void setPublication_date(Date publication_date) {
		this.publication_date = publication_date;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the creators.
	 *
	 * @return the creators
	 */
	public List<? extends ZenodoAuthor> getCreators() {
		return creators;
	}

	/**
	 * Sets the creators.
	 *
	 * @param creators the new creators
	 */
	public void setCreators(List<ZenodoCreator> creators) {
		this.creators = creators;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the access right.
	 *
	 * @return the access right
	 */
	public SerializableEnum<String> getAccess_right() {
		return access_right;
	}

	/**
	 * Sets the access right.
	 *
	 * @param access_right the new access right
	 */
	public void setAccess_right(SerializableEnum<String> access_right) {
		this.access_right = access_right;
	}

	/**
	 * Gets the embargo date.
	 *
	 * @return the embargo date
	 */
	public Date getEmbargo_date() {
		return embargo_date;
	}

	/**
	 * Sets the embargo date.
	 *
	 * @param embargo_date the new embargo date
	 */
	public void setEmbargo_date(Date embargo_date) {
		this.embargo_date = embargo_date;
	}

	/**
	 * Gets the access conditions.
	 *
	 * @return the access conditions
	 */
	public String getAccess_conditions() {
		return access_conditions;
	}

	/**
	 * Sets the access conditions.
	 *
	 * @param access_conditions the new access conditions
	 */
	public void setAccess_conditions(String access_conditions) {
		this.access_conditions = access_conditions;
	}

	/**
	 * Gets the doi.
	 *
	 * @return the doi
	 */
	public String getDoi() {
		return doi;
	}

	/**
	 * Sets the doi.
	 *
	 * @param doi the new doi
	 */
	public void setDoi(String doi) {
		this.doi = doi;
	}

	/**
	 * Gets the preserve doi.
	 *
	 * @return the preserve doi
	 */
	public Boolean getPreserve_doi() {
		return preserve_doi;
	}

	/**
	 * Sets the preserve doi.
	 *
	 * @param preserve_doi the new preserve doi
	 */
	public void setPreserve_doi(Boolean preserve_doi) {
		this.preserve_doi = preserve_doi;
	}

	/**
	 * Gets the keywords.
	 *
	 * @return the keywords
	 */
	public List<String> getKeywords() {
		return keywords;
	}

	/**
	 * Sets the keywords.
	 *
	 * @param keywords the new keywords
	 */
	public void setKeywords(List<String> keywords) {
		this.keywords = keywords;
	}

	/**
	 * Gets the notes.
	 *
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * Sets the notes.
	 *
	 * @param notes the new notes
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * Gets the related identifiers.
	 *
	 * @return the related identifiers
	 */
	public List<ZenodoRelatedIdentifier> getRelated_identifiers() {
		return related_identifiers;
	}

	/**
	 * Sets the related identifiers.
	 *
	 * @param related_identifiers the new related identifiers
	 */
	public void setRelated_identifiers(List<ZenodoRelatedIdentifier> related_identifiers) {
		this.related_identifiers = related_identifiers;
	}

	/**
	 * Gets the contributors.
	 *
	 * @return the contributors
	 */
	public List<? extends ZenodoAuthor> getContributors() {
		return contributors;
	}

	/**
	 * Sets the contributors.
	 *
	 * @param contributors the new contributors
	 */
	public void setContributors(List<ZenodoContributor> contributors) {
		this.contributors = contributors;
	}

	/**
	 * Gets the references.
	 *
	 * @return the references
	 */
	public List<String> getReferences() {
		return references;
	}

	/**
	 * Sets the references.
	 *
	 * @param references the new references
	 */
	public void setReferences(List<String> references) {
		this.references = references;
	}

	/**
	 * Gets the communities.
	 *
	 * @return the communities
	 */
	public List<ZenodoCommunity> getCommunities() {
		return communities;
	}

	/**
	 * Sets the communities.
	 *
	 * @param communities the new communities
	 */
	public void setCommunities(List<ZenodoCommunity> communities) {
		this.communities = communities;
	}

	/**
	 * Gets the grants.
	 *
	 * @return the grants
	 */
	public List<ZenodoGrant> getGrants() {
		return grants;
	}

	/**
	 * Sets the grants.
	 *
	 * @param grants the new grants
	 */
	public void setGrants(List<ZenodoGrant> grants) {
		this.grants = grants;
	}

	/**
	 * Gets the journal title.
	 *
	 * @return the journal title
	 */
	public String getJournal_title() {
		return journal_title;
	}

	/**
	 * Sets the journal title.
	 *
	 * @param journal_title the new journal title
	 */
	public void setJournal_title(String journal_title) {
		this.journal_title = journal_title;
	}

	/**
	 * Gets the journal volume.
	 *
	 * @return the journal volume
	 */
	public String getJournal_volume() {
		return journal_volume;
	}

	/**
	 * Sets the journal volume.
	 *
	 * @param journal_volume the new journal volume
	 */
	public void setJournal_volume(String journal_volume) {
		this.journal_volume = journal_volume;
	}

	/**
	 * Gets the journal issue.
	 *
	 * @return the journal issue
	 */
	public String getJournal_issue() {
		return journal_issue;
	}

	/**
	 * Sets the journal issue.
	 *
	 * @param journal_issue the new journal issue
	 */
	public void setJournal_issue(String journal_issue) {
		this.journal_issue = journal_issue;
	}

	/**
	 * Gets the journal pages.
	 *
	 * @return the journal pages
	 */
	public String getJournal_pages() {
		return journal_pages;
	}

	/**
	 * Sets the journal pages.
	 *
	 * @param journal_pages the new journal pages
	 */
	public void setJournal_pages(String journal_pages) {
		this.journal_pages = journal_pages;
	}

	/**
	 * Gets the conference title.
	 *
	 * @return the conference title
	 */
	public String getConference_title() {
		return conference_title;
	}

	/**
	 * Sets the conference title.
	 *
	 * @param conference_title the new conference title
	 */
	public void setConference_title(String conference_title) {
		this.conference_title = conference_title;
	}

	/**
	 * Gets the conference acronym.
	 *
	 * @return the conference acronym
	 */
	public String getConference_acronym() {
		return conference_acronym;
	}

	/**
	 * Sets the conference acronym.
	 *
	 * @param conference_acronym the new conference acronym
	 */
	public void setConference_acronym(String conference_acronym) {
		this.conference_acronym = conference_acronym;
	}

	/**
	 * Gets the conference dates.
	 *
	 * @return the conference dates
	 */
	public String getConference_dates() {
		return conference_dates;
	}

	/**
	 * Sets the conference dates.
	 *
	 * @param conference_dates the new conference dates
	 */
	public void setConference_dates(String conference_dates) {
		this.conference_dates = conference_dates;
	}

	/**
	 * Gets the conference place.
	 *
	 * @return the conference place
	 */
	public String getConference_place() {
		return conference_place;
	}

	/**
	 * Sets the conference place.
	 *
	 * @param conference_place the new conference place
	 */
	public void setConference_place(String conference_place) {
		this.conference_place = conference_place;
	}

	/**
	 * Gets the conference url.
	 *
	 * @return the conference url
	 */
	public String getConference_url() {
		return conference_url;
	}

	/**
	 * Sets the conference url.
	 *
	 * @param conference_url the new conference url
	 */
	public void setConference_url(String conference_url) {
		this.conference_url = conference_url;
	}

	/**
	 * Gets the conference session.
	 *
	 * @return the conference session
	 */
	public String getConference_session() {
		return conference_session;
	}

	/**
	 * Sets the conference session.
	 *
	 * @param conference_session the new conference session
	 */
	public void setConference_session(String conference_session) {
		this.conference_session = conference_session;
	}

	/**
	 * Gets the conference session part.
	 *
	 * @return the conference session part
	 */
	public String getConference_session_part() {
		return conference_session_part;
	}

	/**
	 * Sets the conference session part.
	 *
	 * @param conference_session_part the new conference session part
	 */
	public void setConference_session_part(String conference_session_part) {
		this.conference_session_part = conference_session_part;
	}

	/**
	 * Gets the imprint publisher.
	 *
	 * @return the imprint publisher
	 */
	public String getImprint_publisher() {
		return imprint_publisher;
	}

	/**
	 * Sets the imprint publisher.
	 *
	 * @param imprint_publisher the new imprint publisher
	 */
	public void setImprint_publisher(String imprint_publisher) {
		this.imprint_publisher = imprint_publisher;
	}

	/**
	 * Gets the imprint isbn.
	 *
	 * @return the imprint isbn
	 */
	public String getImprint_isbn() {
		return imprint_isbn;
	}

	/**
	 * Sets the imprint isbn.
	 *
	 * @param imprint_isbn the new imprint isbn
	 */
	public void setImprint_isbn(String imprint_isbn) {
		this.imprint_isbn = imprint_isbn;
	}

	/**
	 * Gets the imprint place.
	 *
	 * @return the imprint place
	 */
	public String getImprint_place() {
		return imprint_place;
	}

	/**
	 * Sets the imprint place.
	 *
	 * @param imprint_place the new imprint place
	 */
	public void setImprint_place(String imprint_place) {
		this.imprint_place = imprint_place;
	}

	/**
	 * Gets the partof title.
	 *
	 * @return the partof title
	 */
	public String getPartof_title() {
		return partof_title;
	}

	/**
	 * Sets the partof title.
	 *
	 * @param partof_title the new partof title
	 */
	public void setPartof_title(String partof_title) {
		this.partof_title = partof_title;
	}

	/**
	 * Gets the partof pages.
	 *
	 * @return the partof pages
	 */
	public String getPartof_pages() {
		return partof_pages;
	}

	/**
	 * Sets the partof pages.
	 *
	 * @param partof_pages the new partof pages
	 */
	public void setPartof_pages(String partof_pages) {
		this.partof_pages = partof_pages;
	}

	/**
	 * Gets the thesis supervisors.
	 *
	 * @return the thesis supervisors
	 */
	public List<ZenodoCreator> getThesis_supervisors() {
		return thesis_supervisors;
	}

	/**
	 * Sets the thesis supervisors.
	 *
	 * @param thesis_supervisors the new thesis supervisors
	 */
	public void setThesis_supervisors(List<ZenodoCreator> thesis_supervisors) {
		this.thesis_supervisors = thesis_supervisors;
	}

	/**
	 * Gets the thesis university.
	 *
	 * @return the thesis university
	 */
	public String getThesis_university() {
		return thesis_university;
	}

	/**
	 * Sets the thesis university.
	 *
	 * @param thesis_university the new thesis university
	 */
	public void setThesis_university(String thesis_university) {
		this.thesis_university = thesis_university;
	}

	/**
	 * Gets the subjects.
	 *
	 * @return the subjects
	 */
	public List<ZenodoSubject> getSubjects() {
		return subjects;
	}

	/**
	 * Sets the subjects.
	 *
	 * @param subjects the new subjects
	 */
	public void setSubjects(List<ZenodoSubject> subjects) {
		this.subjects = subjects;
	}

	/**
	 * Gets the version.
	 *
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Sets the version.
	 *
	 * @param version the new version
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * Gets the language.
	 *
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * Sets the language.
	 *
	 * @param language the new language
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * Gets the locations.
	 *
	 * @return the locations
	 */
	public List<ZenodoLocation> getLocations() {
		return locations;
	}

	/**
	 * Sets the locations.
	 *
	 * @param locations the new locations
	 */
	public void setLocations(List<ZenodoLocation> locations) {
		this.locations = locations;
	}

	/**
	 * Gets the dates.
	 *
	 * @return the dates
	 */
	public List<ZenodoDateInterval> getDates() {
		return dates;
	}

	/**
	 * Sets the dates.
	 *
	 * @param dates the new dates
	 */
	public void setDates(List<ZenodoDateInterval> dates) {
		this.dates = dates;
	}

	/**
	 * Gets the method.
	 *
	 * @return the method
	 */
	public String getMethod() {
		return method;
	}

	/**
	 * Sets the method.
	 *
	 * @param method the new method
	 */
	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * Sets the license IDs.
	 *
	 * @param licensesIDs the new license IDs
	 */
	public void setLicenseIDs(List<String> licensesIDs) {
		this.licenseIDs = licensesIDs;
	}

	/**
	 * Gets the license IDs.
	 *
	 * @return the license IDs
	 */
	public List<String> getLicenseIDs() {
		return licenseIDs;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ZenodoMetadata [upload_type=");
		builder.append(upload_type);
		builder.append(", publication_type=");
		builder.append(publication_type);
		builder.append(", image_type=");
		builder.append(image_type);
		builder.append(", publication_date=");
		builder.append(publication_date);
		builder.append(", title=");
		builder.append(title);
		builder.append(", creators=");
		builder.append(creators);
		builder.append(", contributors=");
		builder.append(contributors);
		builder.append(", thesis_supervisors=");
		builder.append(thesis_supervisors);
		builder.append(", description=");
		builder.append(description);
		builder.append(", access_right=");
		builder.append(access_right);
		builder.append(", embargo_date=");
		builder.append(embargo_date);
		builder.append(", access_conditions=");
		builder.append(access_conditions);
		builder.append(", doi=");
		builder.append(doi);
		builder.append(", preserve_doi=");
		builder.append(preserve_doi);
		builder.append(", keywords=");
		builder.append(keywords);
		builder.append(", notes=");
		builder.append(notes);
		builder.append(", related_identifiers=");
		builder.append(related_identifiers);
		builder.append(", references=");
		builder.append(references);
		builder.append(", communities=");
		builder.append(communities);
		builder.append(", grants=");
		builder.append(grants);
		builder.append(", journal_title=");
		builder.append(journal_title);
		builder.append(", journal_volume=");
		builder.append(journal_volume);
		builder.append(", journal_issue=");
		builder.append(journal_issue);
		builder.append(", journal_pages=");
		builder.append(journal_pages);
		builder.append(", conference_title=");
		builder.append(conference_title);
		builder.append(", conference_acronym=");
		builder.append(conference_acronym);
		builder.append(", conference_dates=");
		builder.append(conference_dates);
		builder.append(", conference_place=");
		builder.append(conference_place);
		builder.append(", conference_url=");
		builder.append(conference_url);
		builder.append(", conference_session=");
		builder.append(conference_session);
		builder.append(", conference_session_part=");
		builder.append(conference_session_part);
		builder.append(", imprint_publisher=");
		builder.append(imprint_publisher);
		builder.append(", imprint_isbn=");
		builder.append(imprint_isbn);
		builder.append(", imprint_place=");
		builder.append(imprint_place);
		builder.append(", partof_title=");
		builder.append(partof_title);
		builder.append(", partof_pages=");
		builder.append(partof_pages);
		builder.append(", thesis_university=");
		builder.append(thesis_university);
		builder.append(", subjects=");
		builder.append(subjects);
		builder.append(", version=");
		builder.append(version);
		builder.append(", language=");
		builder.append(language);
		builder.append(", locations=");
		builder.append(locations);
		builder.append(", dates=");
		builder.append(dates);
		builder.append(", method=");
		builder.append(method);
		builder.append(", licenseIDs=");
		builder.append(licenseIDs);
		builder.append("]");
		return builder.toString();
	}

}
