//package org.gcube.portlets.widgets.ckan2zenodopublisher.shared.wrapped;
//
//
///**
// * The Interface ZenodoLicense.
// *
// * @author Francesco Mangiacrapa at ISTI-CNR (francesco.mangiacrapa@isti.cnr.it)
// * 
// * Jan 21, 2020
// */
//public interface ZenodoLicense {
//	
//	/**
//	 * Gets the id.
//	 *
//	 * @return the id
//	 */
//	String getId();
//	
//	/**
//	 * Gets the title.
//	 *
//	 * @return the title
//	 */
//	String getTitle();
//	
//	/**
//	 * Gets the url.
//	 *
//	 * @return the url
//	 */
//	String getUrl();
//
//}
