package org.gcube.portlets.widgets.ckan2zenodopublisher.client;

import java.time.Instant;
import java.util.Date;

import javax.ws.rs.client.Entity;

import org.gcube.data.publishing.ckan2zenodo.Fixer;
import org.gcube.data.publishing.ckan2zenodo.model.faults.ZenodoException;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.DepositionMetadata;
import org.gcube.data.publishing.ckan2zenodo.model.zenodo.ZenodoDeposition;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class Tester {

	public static String expression = "^\\/dataset(\\?([a-zA-Z0-9_.-]*.+))*";
	public static String toMatch = "/dataset?systemtype=E39_Actor&groups=huma_num___nakala";
	
	private static ObjectMapper mapper = new ObjectMapper();

	static {
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
		mapper.configure(SerializationFeature.WRITE_EMPTY_JSON_ARRAYS, false);
		mapper.setSerializationInclusion(Include.NON_NULL);
		//		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
	}

//	public static void main(String[] args) {
//		Pattern p = Pattern.compile(expression);
//		Matcher m = p.matcher(toMatch);
//		boolean b = m.matches();
//		
//		System.out.println(b);
//
//	}
	
	
	public static void main(String[] args) {
		
		//ObjectMapper mapper = new ObjectMapper();
		ZenodoDeposition zed = new ZenodoDeposition();
		DepositionMetadata metadata = new DepositionMetadata();
		metadata.setPublication_date(Date.from(Instant.now()));
		//zed.setMetadata(metadata);
		
		String serialized;
		try {
			System.out.println("init json: "+mapper.writeValueAsString(metadata));
			
			serialized = "{\"metadata\":"+Fixer.fixIncoming(mapper.writeValueAsString(metadata))+"}";
			System.out.println("serialized: "+serialized);
			
			Entity<String> JSON = Entity.json(serialized);
			System.out.println("JSON: "+JSON);
			ZenodoDeposition object = mapper.readValue(Fixer.fixIncoming(serialized), ZenodoDeposition.class);
			System.out.println("ZenodoDeposition: "+object);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//System.out.println(metadata.getPublication_date());
		
//		try {
//			updateMetadata(metadata);
//		} catch (ZenodoException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	
	private static ZenodoDeposition updateMetadata(DepositionMetadata meta) throws ZenodoException {		
		try{
			String serialized="{\"metadata\":"+Fixer.fixIncoming(mapper.writeValueAsString(meta))+"}";
			System.out.println("serialized: "+serialized);
		try {
//			Response resp = getWebClient().target(credentials.getBaseUrl()).
//					path(DEPOSITION_BASE_URL).path(depositionId+"").
//					queryParam(ACCESS_TOKEN, credentials.getKey()).request(CONTENT_TYPE)
//					.put(Entity.json(serialized));
//			return check(resp,ZenodoDeposition.class);
			
			Entity<String> JSON = Entity.json(serialized);
			System.out.println("JSON: "+JSON);
			return mapper.readValue(Fixer.fixIncoming(serialized), ZenodoDeposition.class);
		}catch(Throwable t) {
			System.out.println("Error while tryin to update "+serialized);
			return null;
		}		
		}catch(JsonProcessingException e) {
			System.out.println("Error while parsing "+meta + " " +e);
			throw new ZenodoException("Internal error.",e);
		}
		
	}

}
